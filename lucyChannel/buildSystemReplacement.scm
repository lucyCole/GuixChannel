; for each build system there are ignores
; there is the build system
; there are native input additions
; there are argument replacements
( define-module
	( lucyChannel buildSystemReplacement)
	#:use-module ( ice-9 pretty-print)
	#:use-module ( guix build-system)
	#:use-module ( guix build utils)
	#:use-module ( guix build-system python)
	#:use-module ( gnu packages python-build)
	#:use-module ( lucyChannel packages shiny-python)
	#:use-module ( guix packages)
	#:use-module ( guix utils)
	#:use-module ( gnu packages build-tools)
	#:use-module ( guix build-system meson)

	#:export (
		debugPackageAlteration
		packageInputFallback
		adaptToBuildSystem
		createBuildSystemReplacement

		replacePythonBuildSystem
		recursivelyReplacePythonInputs
		replacePythonInputs
		replacePython

		noTests
	)
)

( define
	(
		debugPackageAlteration
		name
		package
	)

	( display name)
	( display "\n")
	( display "NATIVE INPUTS:\n")
	( pretty-print
		( package-native-inputs
			package
		)
	)
	( display "\n")
	( display "INPUTS:\n")
	( pretty-print
		( package-inputs
			package
		)
	)
	( display "\n")
	( display "PROPAGATED INPUTS:\n")
	( pretty-print
		( package-propagated-inputs
			package
		)
	)
	( display "\n")
	( display "ARGUMENTS:\n")
	( pretty-print
		( package-arguments
			package
		)
	)
	( display "\n\n")

)

( define
	(
		packageInputFallback
		inputType
		package
	)
	( case
		inputType
		(
			'native
			( package-native-inputs package)
		)
		(
			'input
			( package-inputs package)
		)
		(
			'propagated
			( package-propagated-inputs package)
		)
	)
)
( define
	(
		adaptToBuildSystem
		package
		buildSystemName
		ignores
		inputModification
		argumentAdditions
	)
	; ( display "\n\n")
	; ( display
	; 	( package-name package)
	; )
	; ( display "\n")
	; ( display
	; 	( package-arguments package)
	; )
	; ( display "\n")
	; ( display argumentAdditions)
	; ( display "\n")
	; ( display
	; 	( ensure-keyword-arguments
	; 		( package-arguments package)
	; 		argumentAdditions
	; 	)
	; )
	; ( display "\n")
	; ( display ignores)
	; ( display "\n")
	; ( display
	; 	( package-build-system package)
	; )
	; ( display "\n")
	; ( display buildSystem)
	; ( display "\n")
	; ( display
	; 	( eq?
	; 		( package-build-system package)
	; 		buildSystem
	; 	)
	; )
	; ( display "\n")
	; ( display
	; 	( eqv?
	; 		( package-build-system package)
	; 		buildSystem
	; 	)
	; )
	; ( display "\n")
	; ( display
	; 	( equal?
	; 		( build-system-name
	; 			( package-build-system package)
	; 		)
	; 		( build-system-name
	; 			buildSystem
	; 		)
	; 	)
	; )

	; ( display "\n")
	; ( display "OPERATING ON: ")
	; ( display
	; 	( and
	; 		( not
	; 			( memq
	; 				package
	; 				ignores
	; 			)
	; 		)
	; 		( equal?
	; 			( build-system-name
	; 				( package-build-system package)
	; 			)
	; 			buildSystemName
	; 		)
	; 	)
	; )
	; ( display "\n\n")

	( if
		( memq
			package
			ignores
		)
		package
		( if
			( equal?
				( build-system-name
					( package-build-system package)
				)
				buildSystemName
			)
			( package/inherit
				package
				( location
					( package-location package)
				)
				( native-inputs
					( inputModification
						'native
						package
					)
				)
				( inputs
					( inputModification
						'input
						package
					)
				)
				( propagated-inputs
					( inputModification
						'propagated
						package
					)
				)
				( arguments
					( ensure-keyword-arguments
						( package-arguments package)
						argumentAdditions
					)
				)
			)
			package
		)
	)

)
( define
	(
		createBuildSystemReplacement
		buildSystem
		ignores
		inputModification
		argumentAdditions
	)
	( define
		recursiveFunction
		( package-mapping
			( lambda
				( package)
				( adaptToBuildSystem
					package
					( build-system-name buildSystem)
					ignores
					inputModification
					argumentAdditions
				)
			)
		)
	)
	( lambda*
		(
			package
			#:key
				( recursive #true)
		)
		( if
			recursive
			( recursiveFunction package)
			( adaptToBuildSystem
				package
				( build-system-name buildSystem)
				ignores
				inputModification
				argumentAdditions
			)
		)
	)
)

( define
	replacePythonBuildSystem
	( createBuildSystemReplacement
		python-build-system
		( list
			python312-setuptools
			python-setuptools
		)
		( lambda
			(
				inputType
				package
			)
			( case
				inputType
				(
					'input
					( modify-inputs
						( package-inputs package)
						; ( package-native-inputs package)
						( append python312-setuptools)
					)
				)
				( else
					( packageInputFallback
						inputType
						package
					)
				)
			)
		)
		`(
			#:python ,( wrapPython3 python-3.12)
			; #:python ,( wrapPython3 python-3.12-withSetuptools)
		)
	)
)
( define
	recursivelyReplacePythonInputs
	( package-input-rewriting/spec
		`(
			( "python" . ,( const python-3.12-withSetuptools))
		)
	)
)
( define*
	(
		replacePythonInputs
		package
		#:key
			( recursive #true)
	)
	( if
		recursive
		( recursivelyReplacePythonInputs package)
		( package/inherit
			package
			( native-inputs
				( modify-inputs
					( package-native-inputs package)
					( replace
						"python"
						python-3.12-withSetuptools
					)
				)
			)
			( inputs
				( modify-inputs
					( package-inputs package)
					( replace
						"python"
						python-3.12-withSetuptools
					)
				)
			)
			( propagated-inputs
				( modify-inputs
					( package-propagated-inputs package)
					( replace
						"python"
						python-3.12-withSetuptools
					)
				)
			)
		)
	)
)
( define*
	(
		replacePython
		package
		#:key
			( recursive #true)
	)
	( replacePythonInputs
		( replacePythonBuildSystem
			package
			#:recursive recursive
		)
		#:recursive recursive
	)
)


; ( define
; 	python312-meson
; 	( replacePython meson)
; 	; ( package
; 	; 	( inherit
; 	; 		( replacePython meson)
; 	; 	)
; 	; 	( name "python312-meson")
; 	; )
; )
; ( define
; 	replaceMesonBuildSystem
; 	( createBuildSystemReplacement
; 		meson-build-system
; 		( list
; 			; python312-setuptools
; 			; python-setuptools
; 		)
; 		packageInputFallback
; 		; ( lambda
; 		; 	(
; 		; 		inputType
; 		; 		package
; 		; 	)
; 		; 	( case
; 		; 		inputType
; 		; 		(
; 		; 			'native
; 		; 			( modify-inputs
; 		; 				( package-native-inputs package)
; 		; 				( append python312-setuptools)
; 		; 			)
; 		; 		)
; 		; 		( else
; 		; 			( packageInputFallback
; 		; 				inputType
; 		; 				package
; 		; 			)
; 		; 		)
; 		; 	)
; 		; )
; 		`(
; 			#:meson ,python312-meson
; 		)
; 	)
; )


( define
	(
		noTests
		package
	)
	( package/inherit
		package
		( arguments
			( ensure-keyword-arguments
				( package-arguments package)
				`(
					#:tests? #false
				)
			)
		)
	)
)
; ( debugPackageAlteration
; 	"BASE"
; 	python-pygobject
; )
; ( debugPackageAlteration
; 	"PYTHON REPLACED"
; 	( replacePython python-pygobject)
; )
; ( debugPackageAlteration
; 	"MESON BUILD SYSTEM REPLACED"
; 	( replaceMesonBuildSystem python-pygobject)
; )
; ( debugPackageAlteration
; 	"BOTH"
; 	( replaceMesonBuildSystem
; 		( replacePython python-pygobject)
; 	)
; )
; ( debugPackageAlteration
; 	"BOTH REPLACEMENT"
; 	( replaceMesonBuildSystem
; 		( package
; 			( inherit ( replacePython python-pygobject))
; 			( name "ydd")
; 		)
; 	)
; )
; ( display
; 	( class-of
; 		( package-arguments python-pygobject)
; 	)
; )
; ( display
; 	( pair?
; 		( package-arguments python-pygobject)
; 	)
; )
; ( display "\n")
; ( display
; 	( car
; 		( package-arguments python-pygobject)
; 	)
; )
; ( display "\n")
; ( display
; 	( class-of
; 		( car
; 			( package-arguments python-pygobject)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( cdr
; 		( package-arguments python-pygobject)
; 	)
; )
; ( display "\n\n")
; ( display
; 	( class-of
; 		( package-arguments
; 			( replaceMesonBuildSystem
; 				( replacePython python-pygobject)
; 			)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( package-arguments
; 		( replaceMesonBuildSystem
; 			( replacePython python-pygobject)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( car
; 		( package-arguments
; 			( replaceMesonBuildSystem
; 				( replacePython python-pygobject)
; 			)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( cdr
; 		( package-arguments
; 			( replaceMesonBuildSystem
; 				( replacePython python-pygobject)
; 			)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( class-of
; 		( cdr
; 			( package-arguments
; 				( replaceMesonBuildSystem
; 					( replacePython python-pygobject)
; 				)
; 			)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( car
; 		( cdr
; 			( package-arguments
; 				( replaceMesonBuildSystem
; 					( replacePython python-pygobject)
; 				)
; 			)
; 		)
; 	)
; )
; ( display "\n")
; ( display
; 	( cdr
; 		( cdr
; 			( package-arguments
; 				( replaceMesonBuildSystem
; 					( replacePython python-pygobject)
; 				)
; 			)
; 		)
; 	)
; )
; ( define
; 	messy
; 	( get-keyword
; 		#:meson
; 		( package-arguments
; 			( replaceMesonBuildSystem
; 				( replacePython python-pygobject)
; 			)
; 		)
; 	)
; )
; ( display "\n\n")
; ( display
; 	( debugPackageAlteration
; 		"MESSY"
; 		messy
; 	)
; )
; ( replaceMesonBuildSystem
; 	; python-pygobject
; 	( replacePython python-pygobject)
; )

; ( define
; 	replacePythonBuildSystem
; 	( package-mapping
; 		( lambda
; 			( package)
; 			( if
; 				; ( eqv?
; 				; 	( package-name package)
; 				; 	"python312-setuptools"
; 				; )
; 				( or
; 					( eq?
; 						package
; 						python312-setuptools
; 					)
; 					( eq?
; 						package
; 						python-setuptools
; 					)
; 					; ( eq?
; 					; 	package
; 					; 	python-3.12
; 					; )
; 					; ( eq?
; 					; 	package
; 					; 	python-3.10
; 					; )
; 				)
; 				package
; 				( let
; 					(
; 						(
; 							replacedBuildSystem
; 							( if
; 								( eq?
; 									( package-build-system package)
; 									python-build-system
; 								)
; 								; ( begin
; 								; ( display "REWRITTEN")
; 								; ( display ( package-name package))

; 								( package/inherit
; 									package
; 									( location
; 										( package-location
; 											package
; 										)
; 									)
; 									( name
; 										( let
; 											(
; 												(
; 													name
; 													( package-name package)
; 												)
; 											)
; 											( string-append
; 												newPrefix
; 												( if
; 													( string-prefix?
; 														oldPrefix
; 														name
; 													)
; 													( substring
; 														name
; 														( string-length oldPrefix)
; 													)
; 													name
; 												)
; 											)
; 										)
; 									)
; 									( native-inputs
; 										( modify-inputs
; 											( package-native-inputs package)
; 											; ( append python-setuptools)
; 											( append python312-setuptools)
; 										)
; 									)
; 									( arguments
; 										( ensure-keyword-arguments
; 											( package-arguments package)
; 											`(
; 												#:python ,( wrap-python3 python-3.12)
; 											)
; 										)
; 										; ( let
; 										; 	(
; 										; 		(
; 										; 			python
; 										; 			( if
; 										; 				( promise? python)
; 										; 				( force python)
; 										; 				python
; 										; 			)
; 										; 		)
; 										; 	)
; 										; 	( ensure-keyword-arguments
; 										; 		( package-arguments package)
; 										; 		`( #:python ,python)
; 										; 	)
; 										; )
; 									)
; 								; )
; 								)
; 								package
; 							)
; 						)
; 					)
; 					replacedBuildSystem
; 					; ( package/inherit
; 					; 	replacedBuildSystem
; 					; 	( inputs
; 					; 		( replacePythonInInputs
; 					; 			( package-inputs package)
; 					; 		)
; 					; 	)
; 					; 	( native-inputs
; 					; 		( replacePythonInInputs
; 					; 			( package-native-inputs package)
; 					; 		)
; 					; 	)
; 					; 	( propagated-inputs
; 					; 		( replacePythonInInputs
; 					; 			( package-propagated-inputs package)
; 					; 		)
; 					; 	)
; 					; )
; 				)
; 			)
; 		)
; 	)
; )
; ( define
; 	(
; 		generateBuildSystemReplacement
; 		buildSystem
; 		argument
; 	)
; )
