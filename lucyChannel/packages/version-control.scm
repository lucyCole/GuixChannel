( define-module
	( lucyChannel packages version-control)

	#:use-module ( guix git-download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system go)
	#:use-module ( guix build utils)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module ( gnu packages xdisorg)
	#:export (
		lazygit
	)
)
( define
	lazygit
	( let
		(
			( projectVersion "0.40.2")
			( revision "0")
			( commit "5e388e21c8ca6aa883dbcbe45c47f6fdd5116815")
		)
		( package
			( name "lazygit")
			( version
				( git-version
					projectVersion
					revision
					commit
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://github.com/jesseduffield/lazygit.git")
							; ( recursive #f)
						)
					)
					( sha256
						( base32 "049gdbhsaqdpj4bn0k4dbzvwaig40p5vhmnqvj09as3f0wl5cgn6")
					)
				)
			)
			( build-system go-build-system)
			( arguments
				( list
					#:import-path "github.com/jesseduffield/lazygit"
					#:build-flags #~( list
						; "-ldflags=-s -w"
						( string-append
							"-ldflags="
							"-X main.version="
							#$projectVersion
							" -X main.buildSource=nix"
						)
					)
				)
			)
			( native-inputs
				( list
				)
			)
			( inputs
				( list
				)
			)
			( propagated-inputs
				( list
				)
			)
			( home-page "https://github.com/jesseduffield/lazygit")
			( synopsis "Simple terminal UI for git commands")
			( description
				"Rant time: You've heard it before, git is powerful, but what good is that power when everything is so damn hard to do? Interactive rebasing requires you to edit a goddamn TODO file in your editor? Are you kidding me? To stage part of a file you need to use a command line program to step through each hunk and if a hunk can't be split down any further but contains code you don't want to stage, you have to edit an arcane patch file by hand? Are you KIDDING me?! Sometimes you get asked to stash your changes when switching branches only to realise that after you switch and unstash that there weren't even any conflicts and it would have been fine to just checkout the branch directly? YOU HAVE GOT TO BE KIDDING ME!

If you're a mere mortal like me and you're tired of hearing how powerful git is when in your daily life it's a powerful pain in your ass, lazygit might be for you."
			)
			( license license.expat)
		)
	)
)
