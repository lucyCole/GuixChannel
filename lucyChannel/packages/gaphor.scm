
( define-module
	( lucyChannel packages gaphor)
	#:use-module ( guix gexp)
	#:use-module (
		( guix licenses)
		#:prefix license:
	)
	#:use-module ( guix packages)
	#:use-module ( guix build utils)
	#:use-module ( gnu packages build-tools)
	#:use-module ( guix build-system python)
	#:use-module ( guix build-system pyproject)
	#:use-module ( guix build pyproject-build-system)
	#:use-module ( guix download)
	#:use-module ( guix utils)

	#:use-module ( gnu packages python-build)
	#:use-module ( gnu packages python-web)
	#:use-module ( gnu packages python-xyz)
	#:use-module ( gnu packages graphviz)
	#:use-module ( gnu packages xml)
	#:use-module ( gnu packages check)
	#:use-module ( gnu packages python-check)
	#:use-module ( gnu packages image)
	#:use-module ( gnu packages compression)
	#:use-module ( gnu packages sphinx)
	#:use-module ( gnu packages gnome)
	#:use-module ( gnu packages pkg-config)
	#:use-module ( gnu packages gtk)
	#:use-module ( gnu packages glib)
	#:export (
		gaphor
	)
)

(define python-furo
  (package
    (name "python-furo")
    (version "2024.1.29")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "furo" version))
       (sha256
        (base32 "00hyiangrdq3fn385hxkqwzs5mrqxdxiwk62kkmkcvhay7ijyssd"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-beautifulsoup4 python-pygments
                             python-sphinx python-sphinx-basic-ng))
    (home-page "")
    (synopsis "A clean customisable Sphinx documentation theme.")
    (description
     "This package provides a clean customisable Sphinx documentation theme.")
    (license #f)))

(define python-generic
  (package
    (name "python-generic")
    (version "1.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "generic" version))
       (sha256
        (base32 "1c25grbrslpkkilf94ajna4db51ifil0b9lsa6zdj0888nd2zx9m"))))
    (build-system pyproject-build-system)
    ( arguments
    	( list
    		#:tests? #f
    	)
    )
    (propagated-inputs (list python-exceptiongroup python-sphinx)) ; delete-ed furo
    ( inputs
    	( list
			python-poetry-core
			python-pytest
    	)
    )
    (home-page "https://generic.readthedocs.io/")
    (synopsis "Generic programming library for Python")
    (description "Generic programming library for Python")
   (license license:bsd-3)))
; bump orig pack
 (define python-defusedxml0
  (package
    (name "python-defusedxml")
    (version "0.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "defusedxml" version))
       (sha256
        (base32 "0s9ym98jrd819v4arv9gmcr6mgljhxd9q866sxi5p4c5n4nh7cqv"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/tiran/defusedxml")
    (synopsis "XML bomb protection for Python stdlib modules")
    (description "XML bomb protection for Python stdlib modules")
    (license #f)))

(define python-better-exceptions
  (package
    (name "python-better-exceptions")
    (version "0.3.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "better_exceptions" version))
       (sha256
        (base32 "0fs5nfg7xdbx5hb1h1148kax68g9wn0h7cclx3k08psd8hcbrrp4"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-colorama))
    (home-page "https://github.com/qix-/better-exceptions")
    (synopsis "Pretty and helpful exceptions, automatically")
    (description "Pretty and helpful exceptions, automatically")
    (license #f)))

(define python-dulwich
  (package
    (name "python-dulwich")
    (version "0.21.7")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "dulwich" version))
       (sha256
        (base32 "0s79c3g19m052jbxm66amxv5v60ijx5px4hjmk1q19ff6dlcdsd9"))))
    (build-system pyproject-build-system)
    ( arguments
    	( list
    		#:tests? #f
    	)
    )
    (propagated-inputs (list python-typing-extensions python-urllib3))
    (home-page "")
    (synopsis "Python Git Library")
    (description "Python Git Library")
    (license #f)))

(define python-gaphas
  (package
    (name "python-gaphas")
    (version "4.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "gaphas" version))
       (sha256
        (base32 "0n0kvn03gqlwsffs5mdc557f67kpw0fpymydzpm2hj5zd1irb067"))))
    (build-system pyproject-build-system)
    ( arguments
    	( list
    		#:tests? #f
    	)
    )
    (propagated-inputs (list python-pycairo python-pygobject
                             python-sphinx)) ; delete-ed furo
    ( inputs
    	( list
			python-poetry-core
			python-pytest
    	)
    )
    (home-page "https://gaphas.readthedocs.io/")
    (synopsis "Gaphas is a GTK diagramming widget")
    (description "Gaphas is a GTK diagramming widget")
    (license #f)))


(define python-better-exceptions
  (package
    (name "python-better-exceptions")
    (version "0.3.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "better_exceptions" version))
       (sha256
        (base32 "0fs5nfg7xdbx5hb1h1148kax68g9wn0h7cclx3k08psd8hcbrrp4"))))
    (build-system pyproject-build-system)
	( arguments
		( list
			#:tests? #false
		)
	)
    (propagated-inputs (list python-colorama))
    (home-page "https://github.com/qix-/better-exceptions")
    (synopsis "Pretty and helpful exceptions, automatically")
    (description "Pretty and helpful exceptions, automatically")
    (license #f)))


; (define-public python-nh3
;   (package
;     (name "python-nh3")
;     (version "0.2.17")
;     (source
;      (origin
;        (method url-fetch)
;        (uri (pypi-uri "nh3" version))
;        (sha256
;         (base32 "0a7hrca5bbbrz20cbqy16n8vaxf4v2q1r9zv9vjlbmn334d79l20"))))
;     (build-system pyproject-build-system)
;     (home-page "")
;     (synopsis "Python bindings to the ammonia HTML sanitization library.")
;     (description "Python bindings to the ammonia HTML sanitization library.")
;     (license license:expat)))

; (define-public python-readme-renderer
;   (package
;     (name "python-readme-renderer")
;     (version "43.0")
;     (source
;      (origin
;        (method url-fetch)
;        (uri (pypi-uri "readme_renderer" version))
;        (sha256
;         (base32 "04g3zpa0kp6505h5inylj2npnkydyy3jdmnqxsg504q82hlds60q"))))
;     (build-system pyproject-build-system)
;     (propagated-inputs (list python-docutils python-nh3 python-pygments))
;     (home-page "")
;     (synopsis
;      "readme_renderer is a library for rendering readme descriptions for Warehouse")
;     (description
;      "readme_renderer is a library for rendering readme descriptions for Warehouse")
;     (license license:asl2.0)))

(define python-zope.event
  (package
    (name "python-zope.event")
    (version "5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zope.event" version))
       (sha256
        (base32 "1kb73swvjszramlxljwlcbvrsnknr7icb8mmw9l406w9v7c41i5s"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-setuptools))
    ; (native-inputs (list python-zope.testrunner))
    (home-page "https://github.com/zopefoundation/zope.event")
    (synopsis "Very basic event publishing system")
    (description "Very basic event publishing system")
    (license #f)))

(define python-zope.interface
  (package
    (name "python-zope.interface")
    (version "6.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zope.interface" version))
       (sha256
        (base32 "0smffjifwa0krm28xvw5y00007gyzbxv5m1vdj19lb96495nnggq"))))
    (build-system pyproject-build-system)
	( arguments
		( list
			#:tests? #false
		)
	)
    (propagated-inputs (list python-setuptools))
    (native-inputs (list python-coverage python-zope.event python-zope.testing))
    (home-page "https://github.com/zopefoundation/zope.interface")
    (synopsis "Interfaces for Python")
    (description "Interfaces for Python")
    (license #f)))

(define python-zope.exceptions
  (package
    (name "python-zope.exceptions")
    (version "5.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zope.exceptions" version))
       (sha256
        (base32 "15nx9z15r4pgnxzppcg20qzmxwbpkyvggq1m2ffvspwz6gjm7z1h"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-setuptools python-zope.interface))
    (native-inputs (list python-zope-testrunner-bootstrap))
    (home-page "https://github.com/zopefoundation/zope.exceptions")
    (synopsis "Zope Exceptions")
    (description "Zope Exceptions")
    (license #f)))

(define python-zope.testrunner
  (package
    (name "python-zope.testrunner")
    (version "6.4")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zope.testrunner" version))
       (sha256
        (base32 "1j1828vghdn619bv5jhf54zzf1blxkjdxyddv1fnqavg9p3rz18b"))))
    (build-system pyproject-build-system)
    (arguments
     '(#:tests? #f                    ;FIXME: Tests can't find zope.interface.
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'remove-problematic-test
           (lambda _
             ;; This test contains invalid syntax, which breaks bytecode
             ;; compilation.  For simplicity just remove it.
             (delete-file
              "src/zope/testrunner/tests/testrunner-ex/sample2/badsyntax.py"))))))
    (propagated-inputs (list python-setuptools python-zope.exceptions
                             python-zope.interface))
    (native-inputs (list python-zope.testing))
    (home-page "https://github.com/zopefoundation/zope.testrunner")
    (synopsis "Zope testrunner script.")
    (description "Zope testrunner script.")
    (license #f)))

(define python-zope.testing
  (package
    (name "python-zope.testing")
    (version "5.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zope.testing" version))
       (sha256
        (base32 "0jfnycp9kzmmkk0rard8chd81v5yp6vnm09ky7d3qmv6svcd0z78"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-setuptools))
    ; (native-inputs (list python-zope.testrunner))
    (home-page "https://github.com/zopefoundation/zope.testing")
    (synopsis "Zope testing helpers")
    (description "Zope testing helpers")
    (license #f)))

(define python-zest.releaser
  (package
    (name "python-zest.releaser")
    (version "9.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zest.releaser" version))
       (sha256
        (base32 "00b0mp0m6cq6126d54g43fm9sk31j9q4v7vr7l83wsvzk8jzpwby"))))
    (build-system pyproject-build-system)
	( arguments
		( list
			#:tests? #false
			#:phases `( modify-phases
				%standard-phases
				( delete 'sanity-check)
			)
		)
	)
    (propagated-inputs (list
                       ; python-build
                             python-tomli
                             python-colorama
                             python-readme-renderer
                             python-requests
                             python-setuptools
                             python-twine))
    (native-inputs (list python-wheel python-zope.testing
                         python-zope.testrunner))
    (home-page "")
    (synopsis "Software releasing made easy and repeatable")
    (description "Software releasing made easy and repeatable")
    (license license:gpl3)))

(define python-uv
  (package
    (name "python-uv")
    (version "0.1.31")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "uv" version))
       (sha256
        (base32 "1pv5nsivndmq5a8gz4xvnr8qmfibb0mm9w2j15bglfnzh8jpjvk8"))))
    (build-system pyproject-build-system)
    (home-page "https://pypi.org/project/uv/")
    (synopsis
     "An extremely fast Python package installer and resolver, written in Rust.")
    (description
     "An extremely fast Python package installer and resolver, written in Rust.")
    (license #f)))

(define python-sphinx-argparse-cli
  (package
    (name "python-sphinx-argparse-cli")
    (version "1.14.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "sphinx_argparse_cli" version))
       (sha256
        (base32 "01jsairn61p9jd2hacfcm5ck3rg97a59cwgsrbv29s4q1arvrwk4"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-sphinx))
    (native-inputs (list python-covdefaults python-pytest python-pytest-cov))
    (home-page "")
    (synopsis
     "render CLI arguments (sub-commands friendly) defined by argparse module")
    (description
     "render CLI arguments (sub-commands friendly) defined by argparse module")
    (license #f)))


(define python-build
  (package
    (name "python-build")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "build" version))
       (sha256
        (base32 "17g1wcx8f7h0db3ymwdni1sjnyrpfna5fi9m8dng49hchzs66qjj"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list
                       python-build
                             python-colorama
                             python-filelock
                             ; python-furo
                             python-importlib-metadata
                             python-mypy
                             python-packaging
                             python-pyproject-hooks
                             python-pytest
                             python-pytest-cov
                             python-pytest-mock
                             python-pytest-rerunfailures
                             python-pytest-xdist
                             python-setuptools
                             python-sphinx
                             python-sphinx-argparse-cli
                             python-sphinx-autodoc-typehints
                             python-sphinx-issues
                             python-tomli
                             python-typing-extensions
                             python-uv
                             python-virtualenv
                             python-wheel))
    (home-page "")
    (synopsis "A simple, correct Python build frontend")
    (description
     "This package provides a simple, correct Python build frontend")
    (license #f)))

(define python-pyroma
  (package
    (name "python-pyroma")
    (version "4.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pyroma" version))
       (sha256
        (base32 "0g1qwzqwmkz3k3nfynwr444cm6flxd3zpmdf9wki43m1lz27swkc"))))
    (build-system pyproject-build-system)
	( arguments
		( list
			#:tests? #false
			#:phases `( modify-phases
				%standard-phases
				( delete 'sanity-check)
			)
		)
	)
    (propagated-inputs (list
                       ; python-build
                             python-docutils
                             python-packaging
                             python-pygments
                             python-requests
                             python-setuptools
                             python-trove-classifiers))
    (native-inputs (list python-setuptools python-zest.releaser))
    (home-page "https://github.com/regebro/pyroma")
    (synopsis "Test your project's packaging friendliness")
    (description "Test your project's packaging friendliness")
    (license license:expat)))

(define python-pillow0
  (package
    (name "python-pillow")
    (version "10.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pillow" version))
       (sha256
        (base32 "0babh535kbhr6xs0182k8ahn65qf5nl9ms4kqx04i4acykxma94x"))))
    (build-system python-build-system)
	( arguments
		( list
			#:tests? #false
		)
	)
    (native-inputs (list
				; python-poetry-core
				zlib
				ijg-libjpeg
                   python-check-manifest
                         python-coverage
                         python-defusedxml
                         python-markdown2
                         python-olefile
                         python-packaging
                         python-pyroma
                         python-pytest
                         python-pytest-cov
                         python-pytest-timeout))
    (home-page "")
    (synopsis "Python Imaging Library (Fork)")
    (description "Python Imaging Library (Fork)")
    (license #f)))

( define
	gaphor
	( package
		( name "gaphor")
		( version "")
		( home-page "")
		( synopsis "")
		( description "")
		( license #f)
		( source
			( origin
				( method url-fetch)
				( uri "https://github.com/gaphor/gaphor/archive/refs/tags/2.24.0.tar.gz")
				( sha256
					( base32 "0d3yjw7qh16xwp7v4xl16jyzjqw1j1y0i13541vwddid9i8k43z2")
				)
			)
		)
		( build-system pyproject-build-system)
		( arguments
			( list
				#:tests? #false
				#:modules `(
					( guix build utils)
					( guix build pyproject-build-system)
				)
				#:phases #~( modify-phases
					%standard-phases
					( delete 'sanity-check)
					( add-after
						'install
						'installXDG
						( lambda
							_
							( let*
								(
									( id
										"org.gaphor.Gaphor"
									)
									( iconDir
										( string-append
											#$output
											"/share/icons/hicolor/scalable/apps/"
										)
									)
								)
								( mkdir-p iconDir)
								( copy-file
									( string-append
										"./data/logos/"
										id
										".svg"
									)
									( string-append
										iconDir
										id
										".svg"
									)
								)
								( make-desktop-entry-file
									( string-append
										#$output
										"/share/applications/"
										id
										".desktop"
									)
									#:name "Gaphor"
									#:comment "Gaphor is a UML and SysML modeling application written in Python"
									#:icon id
									#:exec ( string-append
										#$output
										"/bin/gaphor"
									)
									#:categories ( list
										"Development"
									)
								)
							)
						)
					)
				)
			)
		)
		( inputs
			( list
				python-poetry-core
				python-pytest
				python-tinycss2
				python-pygit2
				python-pydot
				python-dulwich
				python-generic
				python-pillow0
				python-better-exceptions
				python-gaphas
				python-babel
				python-jedi
				python-defusedxml0
				( package
					( inherit python-pycairo)
					( version "1.22.0")
					( source
						( origin
							( method url-fetch)
							( uri
								( string-append
									"https://github.com/pygobject/pycairo/releases/download/v"
									version
									"/pycairo-"
									version
									".tar.gz"
								)
							)
							( sha256
								( base32 "0gqzj4zvwdmz28lzsgnwgamkpc6rk2ri5w3l09zlr7b1vymifidk")
							)
						)
					)
				)
				cairo
				gtk
			)
		)
		( native-inputs
			( list
			)
		)
		( propagated-inputs
			( list
				python-pygobject
				gtksourceview
				pkg-config
			)
		)
	)
)
