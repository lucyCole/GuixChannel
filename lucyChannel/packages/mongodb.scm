( define-module
	( lucyChannel packages mongodb)
	#:use-module ( guix gexp)
	#:use-module ( guix git-download)
	#:use-module (
		( guix licenses)
		#:prefix license:
	)
	#:use-module ( guix packages)
	#:use-module ( gnu packages)
	#:use-module ( guix build utils)
	#:use-module ( gnu packages build-tools)
	#:use-module ( guix build-system scons)
	#:use-module ( guix download)
	#:use-module ( guix utils)

	#:use-module ( gnu packages tls)
	#:use-module ( gnu packages pcre)
	#:use-module ( gnu packages databases)
	#:use-module ( gnu packages serialization)
	#:use-module ( gnu packages compression)
	#:use-module ( gnu packages valgrind)
	#:use-module ( gnu packages perl)
	#:use-module ( gnu packages python)
	#:use-module ( gnu packages python-xyz)
	#:use-module ( gnu packages python-build)
	#:use-module ( gnu packages python-web)
	#:use-module ( gnu packages base)
	#:use-module ( gnu packages boost)
	#:use-module ( gnu packages libunwind)
	#:use-module ( gnu packages curl)
	; #:use-module ( gnu packages gsasl)
	#:use-module ( gnu packages cyrus-sasl)
	#:use-module ( gnu packages openldap)
	#:use-module ( gnu packages kerberos)
	#:use-module ( gnu packages language)
	#:use-module ( gnu packages cpp)
	#:use-module ( gnu packages commencement)

	#:use-module ( srfi srfi-1)
	#:use-module ( srfi srfi-26)
	#:export (
		mongodb
	)
)
(define mongodb
  (package
    (name "mongodb")
    (version "7.0.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://fastdl.mongodb.org/src/mongodb-src-r"
                                  version ".tar.gz"))
              (file-name (string-append name "-" version ".tar.gz"))
              (sha256
               (base32 "04hla03yzvlrycz9zx9n93n1yf1mldah9qsyqflh5sdxg49jkywv"))
			( patches
				( search-patches "mongodb-7.0.2-sconstruct.patch")
			)
              ; (modules '((guix build utils)))
              ; (snippet
              ;  '(begin
              ;     (for-each (lambda (dir)
              ;                 (delete-file-recursively
              ;                   (string-append "src/third_party/" dir)))
              ;               '("pcre-8.42" "scons-2.5.0" "snappy-1.1.3"
              ;                 "valgrind-3.11.0" "wiredtiger"
              ;                 "yaml-cpp-0.6.2" "zlib-1.2.11"))
              ;     #t))))
      ))
    (build-system scons-build-system)
    (inputs
      ( list
        libstemmer
        snappy
        boost
        pcre2
        yaml-cpp
        gperftools
        libunwind
        curl
        ( list
          zstd
          "lib"
        )
        zlib
        ; gsasl
        cyrus-sasl
        openssl
        gcc-toolchain
        glibc
        openldap
        mit-krb5
      )
    )
     ; `(("openssl" ,openssl-1.1)
     ;   ("pcre" ,pcre)
     ;    ; ,@(match (%current-system)
     ;    ;     ((or "x86_64-linux" "aarch64-linux" "mips64el-linux")
     ;    ;      `(("wiredtiger" ,wiredtiger)))
     ;    ;     (_ `()))
     ;   ("wiredtiger" ,wiredtiger)
     ;   ("yaml-cpp" ,yaml-cpp)
     ;   ("zlib" ,zlib)
     ;   ("snappy" ,snappy)))
    (native-inputs
      ( list
        python-3.10
        python-psutil
        python-setuptools
        python-regex
        python-cheetah
        python-pyyaml
        python-requests
        python-pymongo
        python-packaging
        valgrind
        ; mongo-c-driver
        tzdata-for-tests
      )
    )
     ; `(("valgrind" ,valgrind)
     ;   ("perl" ,perl)
     ;   ("python" ,python-2)
     ;   ; ("python2-pymongo" ,python2-pymongo)
     ;   ; ("python2-pyyaml" ,python2-pyyaml)
     ;   ("tzdata" ,tzdata-for-tests)))
    (arguments
     `(
       ; #:scons ,scons-python2
       #:tests? #false
       #:phases
       (let ((common-options
              `(
                "--use-system-boost"
                "--use-system-pcre2"
                "--use-system-snappy"
                "--use-system-stemmer"
                "--use-system-yaml"
                "--use-system-zlib"
                "--use-system-zstd"
                "--use-system-tcmalloc"
                "--use-system-libunwind"
                "--use-system-valgrind"
                ; "--use-system-libbson"
                ; "--use-system-mongo-c"

                "--use-sasl-client"
                "--ssl"
                "--disable-warnings-as-errors"
                "--runtime-hardening=off"
                ,(format #f "--jobs=~a" (parallel-job-count))
          )))
         (modify-phases %standard-phases
           (add-after 'unpack 'scons-propagate-environment
             (lambda _
               ;; Modify the SConstruct file to arrange for
               ;; environment variables to be propagated.
               (substitute* "SConstruct"
                 (("^env = Environment\\(")
                  "env = Environment(ENV=os.environ, "))
               #t))
;            (add-after 'unpack 'create-version-file
;              (lambda _
;                (call-with-output-file "version.json"
;                  (lambda (port)
;                    (display ,(simple-format #f "{
;     \"version\": \"~A\"
; }" version) port)))
;                #t))
           (replace 'build
             (lambda _
                  ; not doing test-(.) for now
                  ; https://www.mongodb.com/community/forums/t/running-mongodb-unit-and-dbtests/13933/9
                  ; https://github.com/mongodb/mongo/wiki/Test-The-Mongodb-Server#test-using-resmokepy
             ; (lambda* (#:key tests? inputs #:allow-other-keys)
               (apply invoke `("scons"
                               ,@common-options
                               ; "mongod" "mongo" "mongos"
                               ; ,( if
                               ;   tests?
                               ;   "install-all"
                               ; "install-devcore"
                               ;  )
                               "install-devcore"
              ))))
           (replace 'check
             (lambda* (#:key tests? inputs #:allow-other-keys)
               (setenv "TZDIR"
                       (string-append (assoc-ref inputs "tzdata")
                                      "/share/zoneinfo"))
               (when tests?
                 ;; Note that with the tests, especially the unittests, the
                 ;; build can take up to ~45GB of space, as many tests are
                 ;; individual executable files, with some being hundreds of
                 ;; megabytes in size.
                 ; (apply invoke `("scons" ,@common-options "dbtest" "unittests"))
                 (substitute* "build/unittests.txt"
                   ;; TODO: Don't run the async_stream_test, as it hangs
                   (("^build\\/opt\\/mongo\\/executor\\/async\\_stream\\_test\n$")
                    "")
                   ;; TODO: This test fails
                   ;; Expected 0UL != disks.size() (0 != 0) @src/mongo/util/procparser_test.cpp:476
                   (("^build\\/opt\\/mongo\\/util\\/procparser\\_test\n$")
                    ""))
                 (invoke "python" "buildscripts/resmoke.py"
                         "--suites=dbtest,unittests"
                         (format #f  "--jobs=~a" (parallel-job-count))))
               #t))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let (
                    (bin (string-append (assoc-ref outputs "out") "/bin")))
                 (install-file "./build/install/bin/mongod" bin)
                 (install-file "./build/install/bin/mongos" bin)
                 (install-file "./build/install/bin/mongo" bin))
               #t))))))
    (home-page "https://www.mongodb.org/")
    (synopsis "High performance and high availability document database")
    (description
     "Mongo is a high-performance, high availability, schema-free
document-oriented database.  A key goal of MongoDB is to bridge the gap
between key/value stores (which are fast and highly scalable) and traditional
RDBMS systems (which are deep in functionality).")
    (license (list license:agpl3
                   ;; Some parts are licensed under the Apache License
                   license:asl2.0))))


; (define mongodb
;   (package
;     (name "mongodb")
;     (version "3.4.24")
;     (source (origin
;               (method url-fetch)
;               (uri (string-append "https://github.com/mongodb/mongo/archive/r"
;                                   version ".tar.gz"))
;               (file-name (string-append name "-" version ".tar.gz"))
;               (sha256
;                (base32 "0y1669sqj8wyf0y0njhxs4qhn1qzjhrs2h2qllya5samxrlrjhkg"))
;               (modules '((guix build utils)))
;               (snippet
;                '(begin
;                   (for-each (lambda (dir)
;                               (delete-file-recursively
;                                 (string-append "src/third_party/" dir)))
;                             '("pcre-8.42" "scons-2.5.0" "snappy-1.1.3"
;                               "valgrind-3.11.0" "wiredtiger"
;                               "yaml-cpp-0.6.2" "zlib-1.2.11"))
;                   #t))))
;     (build-system scons-build-system)
;     (inputs
;      `(("openssl" ,openssl-1.1)
;        ("pcre" ,pcre)
;         ; ,@(match (%current-system)
;         ;     ((or "x86_64-linux" "aarch64-linux" "mips64el-linux")
;         ;      `(("wiredtiger" ,wiredtiger)))
;         ;     (_ `()))
;        ("wiredtiger" ,wiredtiger)
;        ("yaml-cpp" ,yaml-cpp)
;        ("zlib" ,zlib)
;        ("snappy" ,snappy)))
;     (native-inputs
;      `(("valgrind" ,valgrind)
;        ("perl" ,perl)
;        ("python" ,python-2)
;        ; ("python2-pymongo" ,python2-pymongo)
;        ; ("python2-pyyaml" ,python2-pyyaml)
;        ("tzdata" ,tzdata-for-tests)))
;     (arguments
;      `(#:scons ,scons-python2
;        #:tests? #false
;        #:phases
;        (let ((common-options
;               `(;; "--use-system-tcmalloc" TODO: Missing gperftools
;                 "--use-system-pcre"
;                 ;; wiredtiger is 64-bit only
;                 ,,(if (any (cute string-prefix? <> (or (%current-target-system)
;                                                        (%current-system)))
;                            '("i686-linux" "armhf-linux"))
;                     ``"--wiredtiger=off"
;                     ``"--use-system-wiredtiger")
;                 ;; TODO
;                 ;; build/opt/mongo/db/fts/unicode/string.o failed: Error 1
;                 ;; --use-system-boost
;                 "--use-system-snappy"
;                 "--use-system-zlib"
;                 "--use-system-valgrind"
;                 ;; "--use-system-stemmer" TODO: Missing relevant package
;                 "--use-system-yaml"
;                 "--disable-warnings-as-errors"
;                 ,(format #f "--jobs=~a" (parallel-job-count))
;                 "--ssl")))
;          (modify-phases %standard-phases
;            (add-after 'unpack 'patch
;              (lambda _
;                ;; Remove use of GNU extensions in parse_number_test.cpp, to
;                ;; allow compiling with GCC 7 or later
;                ;; https://jira.mongodb.org/browse/SERVER-28063
;                (substitute* "src/mongo/base/parse_number_test.cpp"
;                  (("0xabcab\\.defdefP-10")
;                   "687.16784283419838"))
;                #t))
;            (add-after 'unpack 'scons-propagate-environment
;              (lambda _
;                ;; Modify the SConstruct file to arrange for
;                ;; environment variables to be propagated.
;                (substitute* "SConstruct"
;                  (("^env = Environment\\(")
;                   "env = Environment(ENV=os.environ, "))
;                #t))
;            (add-after 'unpack 'create-version-file
;              (lambda _
;                (call-with-output-file "version.json"
;                  (lambda (port)
;                    (display ,(simple-format #f "{
;     \"version\": \"~A\"
; }" version) port)))
;                #t))
;            (replace 'build
;              (lambda _
;                (apply invoke `("scons"
;                                ,@common-options
;                                "mongod" "mongo" "mongos"))))
;            (replace 'check
;              (lambda* (#:key tests? inputs #:allow-other-keys)
;                (setenv "TZDIR"
;                        (string-append (assoc-ref inputs "tzdata")
;                                       "/share/zoneinfo"))
;                (when tests?
;                  ;; Note that with the tests, especially the unittests, the
;                  ;; build can take up to ~45GB of space, as many tests are
;                  ;; individual executable files, with some being hundreds of
;                  ;; megabytes in size.
;                  (apply invoke `("scons" ,@common-options "dbtest" "unittests"))
;                  (substitute* "build/unittests.txt"
;                    ;; TODO: Don't run the async_stream_test, as it hangs
;                    (("^build\\/opt\\/mongo\\/executor\\/async\\_stream\\_test\n$")
;                     "")
;                    ;; TODO: This test fails
;                    ;; Expected 0UL != disks.size() (0 != 0) @src/mongo/util/procparser_test.cpp:476
;                    (("^build\\/opt\\/mongo\\/util\\/procparser\\_test\n$")
;                     ""))
;                  (invoke "python" "buildscripts/resmoke.py"
;                          "--suites=dbtest,unittests"
;                          (format #f  "--jobs=~a" (parallel-job-count))))
;                #t))
;            (replace 'install
;              (lambda* (#:key outputs #:allow-other-keys)
;                (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
;                  (install-file "mongod" bin)
;                  (install-file "mongos" bin)
;                  (install-file "mongo" bin))
;                #t))))))
;     (home-page "https://www.mongodb.org/")
;     (synopsis "High performance and high availability document database")
;     (description
;      "Mongo is a high-performance, high availability, schema-free
; document-oriented database.  A key goal of MongoDB is to bridge the gap
; between key/value stores (which are fast and highly scalable) and traditional
; RDBMS systems (which are deep in functionality).")
;     (license (list license:agpl3
;                    ;; Some parts are licensed under the Apache License
;                    license:asl2.0))))
