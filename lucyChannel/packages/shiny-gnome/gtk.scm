( define-module
	( lucyChannel packages shiny-gnome gtk)

	#:use-module ( guix git-download)
	#:use-module ( guix download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build utils)
	#:use-module ( guix utils)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module (
		( lucyChannel packages shiny-gnome gobject-introspection)
		#:prefix lucyGI.
	)
	#:use-module (
		( lucyChannel packages shiny-gnome glib)
		#:prefix lucyGLib.
	)
	#:use-module ( lucyChannel buildSystemReplacement)

	#:use-module ( gnu packages docbook)
	#:use-module ( gnu packages gettext)
	; #:use-module ( gnu packages gtk) ; harfbuzz
	#:use-module (
		( gnu packages gtk)
		#:prefix gtk.
	)
	#:use-module ( gnu packages bash)
	#:use-module ( gnu packages compression)
	#:use-module ( gnu packages man)
	#:use-module ( gnu packages perl)

	#:use-module ( gnu packages pkg-config)
	#:use-module ( gnu packages python-xyz)
	#:use-module ( gnu packages python)
	#:use-module ( gnu packages python-build)
	#:use-module ( gnu packages web)
	#:use-module ( gnu packages gnome) ; librsvg-for-system
	#:use-module ( gnu packages cups)
	#:use-module ( gnu packages video)
	#:use-module ( gnu packages gstreamer)
	#:use-module ( gnu packages fribidi)
	#:use-module ( gnu packages xml)
	#:use-module ( gnu packages base) ; tzdata-for-tests
	#:use-module ( gnu packages xorg) ; xorg-data-for-tests
	#:use-module ( gnu packages vulkan)
	#:use-module ( gnu packages iso-codes)
	#:use-module ( gnu packages fontutils)
	#:use-module ( gnu packages gl)
	#:use-module ( gnu packages image)
	#:use-module ( gnu packages python)
	#:use-module ( gnu packages freedesktop)
	#:use-module ( gnu packages xdisorg)
	#:use-module (
		( gnu packages glib)
		#:prefix gnuGLib.
	)

	#:export (
		gtk
		gtk4-layer-shell
		libadwaita
	)
)

; Packages to define
; gobject-introspaction >= 1.76.0
; glib >= 2.76.0

; unsure
; gi-docgen or gtk-doc?
; libintl, no version mentioned
; libxslt, not mentioned
; python module-s for cod building
; sassc no version mentioned
; not sure what the vala dep is for
; colord dep version is very different, in bounds but maybe different technique
; ffmpeg requires many different libraries, not sure about versions
; harbuzz version is very different, in bounds but maybe different technique
; iso-codes, no version specified
; libjpeg-turbo, libpng, libtiff no version specified
; tracker version not specified, is above 3
; fontconfig version not specified
; no version specified for x shit that is not xkbcommon
; libxkbcommon is fairly different
( define
	( notest pkg)
	( package
		( inherit pkg)
		( arguments
			( cons*
				#:tests? #false
				( package-arguments pkg)
			)
		)
	)
)

( define
	shinify
	( package-input-rewriting
		`(
			( ,gnuGLib.glib . ,lucyGLib.glib)
			( ,gnuGLib.gobject-introspection . ,lucyGI.gobject-introspection)
			( ,gnuGLib.python-pygobject . ,( notest gnuGLib.python-pygobject))
			( ,gtk.gtk+-2 . ,( notest gtk.gtk+-2))
			( ,gst-plugins-base . ,( notest gst-plugins-base))
		)
	)
)



(define-public pango
  (package
    (name "pango")
    (version "1.52.2")
    (source (origin
              (method url-fetch)
              (uri (string-append "mirror://gnome/sources/pango/"
                                  (version-major+minor version) "/"
                                  name "-" version ".tar.xz"))
              (patches (search-patches "pango-skip-libthai-test.patch"))
              (sha256
               (base32
                "0sx9g0gv8dppizjvh21rx0n5zkly6kwrkv6yafw18a0807z6l1yh"))))
    (build-system meson-build-system)
    (arguments
     '(#:glib-or-gtk? #t             ; To wrap binaries and/or compile schemas
       #:phases (modify-phases %standard-phases
                  (add-after 'unpack 'disable-cantarell-tests
                    (lambda _
                      (substitute* "tests/meson.build"
                        ;; XXX FIXME: These tests require "font-abattis-cantarell", but
                        ;; adding it here would introduce a circular dependency.
                        (("\\[ 'test-layout'.*") "")
                        (("\\[ 'test-itemize'.*") "")
                        (("\\[ 'test-font'.*") "")
                        (("\\[ 'test-harfbuzz'.*") "")))))))
    (propagated-inputs
     ;; These are all in Requires or Requires.private of the '.pc' files.
     (list
			gtk.cairo
           fontconfig
           freetype
           fribidi
           lucyGLib.glib
           gtk.harfbuzz
           gtk.libthai
           ;; Some packages, such as Openbox, expect Pango to be built with the
           ;; optional libxft support.
           libxft
           libxrender))
    (inputs
     (list bash-minimal
           zlib))
    (native-inputs
     (append (list `(,lucyGLib.glib "bin"))      ;glib-mkenums, etc.
             (if (target-hurd?)
                 '()
                 (list lucyGI.gobject-introspection)) ;g-ir-compiler, etc.
             (list
              help2man
              perl
              pkg-config
              python-wrapper)))
    (synopsis "Text and font handling library")
    (description "Pango is a library for laying out and rendering of text, with
an emphasis on internationalization.  Pango can be used anywhere that text
layout is needed, though most of the work on Pango so far has been done in the
context of the GTK+ widget toolkit.  Pango forms the core of text and font
handling for GTK+-2.x.")
    (home-page "https://pango.gnome.org/")
    (license license.lgpl2.0+)))




( define
	gtk
	( let
		(
			( revision "0")
			; ( commit "68755c0fd2bc1b338ffb16e9187830f99a7be20c")
			( commit "4.15.0")
		)
		( package
			( name "gtk")
			( version
				( string-append
					"main"
					"-"
					revision
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://gitlab.gnome.org/GNOME/gtk.git")
							; ( recursive #f)
						)
					)
					( sha256
						( base32 "0hf2g5p65s98qnba2cdw5np5dwacxhiw5dynqd85mpc3izdfmngm")
					)
					( patches
						( search-patches "gtk4-respect-GUIX_GTK4_PATH.patch")
					)
				)
			)
			( build-system meson-build-system)
			( outputs
				'(
					"out"
					"bin"
					"doc"
				)
			)
			( arguments
				( list
					#:tests? #false
					#:modules '(
						( guix build utils)
						( guix build meson-build-system)
						(
							( guix build glib-or-gtk-build-system)
							#:prefix glib-or-gtk:
						)
					)
					#:configure-flags #~( list
						; i think introspection is not built
						"-Dbroadway-backend=true"      ;for broadway display-backend
						"-Dcloudproviders=enabled"     ;for cloud-providers support
						"-Dtracker=enabled"            ;for filechooser search support
						"-Dcolord=enabled"             ;for color printing support
						"-Dvulkan=enabled"
						"-Dintrospection=enabled"

						"-Ddocumentation=true"
						; #$@( if
						; 	( %current-target-system)
						; 	;; If true, gtkdoc-scangobj will try to execute a
						; 	;; cross-compiled binary.
						; 	'( "-Ddocumentation=false")
						; 	'( "-Ddocumentation=true")
						; )

						"-Dman-pages=true"
					)
					#:test-options '( list
						; "--setup=x11" ;defaults to wayland
						"--setup=wayland" ;defaults to wayland
						;; Use the same test options as upstream uses for
						;; their CI.
						"--suite=gtk"
						"--no-suite=gsk-compare-broadway"
					)
					#:phases #~( modify-phases
						%standard-phases
						( add-after
							'unpack
							'generate-gdk-pixbuf-loaders-cache-file
							( assoc-ref
								glib-or-gtk:%standard-phases
								'generate-gdk-pixbuf-loaders-cache-file
							)
						)
						( add-after
							'unpack
							'patch-rst2man
							( lambda _
								( substitute*
									"docs/reference/gtk/meson.build"
									(
										( "find_program\\('rst2man'")
										"find_program('rst2man.py'"
									)
								)
							)
						)
						( add-after
							'unpack
							'patch
							( lambda*
								(
									#:key
										inputs
										native-inputs
										outputs
									#:allow-other-keys
								)
								;; Correct DTD resources of docbook.
								( substitute*
									( find-files
										"docs"
										"\\.xml$"
									)
									(
										( "http://www.oasis-open.org/docbook/xml/4.3/")
										( string-append
											#$( this-package-native-input
												"docbook-xml"
											)
											"/xml/dtd/docbook/"
										)
									)
								)
								;; Disable building of icon cache.
								( substitute*
									"meson.build"
									(
										( "gtk_update_icon_cache: true")
										"gtk_update_icon_cache: false"
									)
								)
								;; Disable failing tests.
								( substitute*
									( find-files
										"testsuite"
										"meson.build"
									)
									(
										( "[ \t]*'empty-text.node',")
										""
									)
									(
										( "[ \t]*'testswitch.node',")
										""
									)
									(
										( "[ \t]*'widgetfactory.node',")
										""
									)
									;; The unaligned-offscreen test fails for unknown reasons, also
									;; on different distributions (see:
									;; https://gitlab.gnome.org/GNOME/gtk/-/issues/4889).
									(
										( "  'unaligned-offscreen',")
										""
									)
								)
								( substitute*
									"testsuite/reftests/meson.build"
									(
										("[ \t]*'label-wrap-justify.ui',")
										""
									)
									;; The inscription-markup.ui fails due to /etc/machine-id
									;; related warnings (see:
									;; https://gitlab.gnome.org/GNOME/gtk/-/issues/5169).
									(
										( "[ \t]*'inscription-markup.ui',")
										""
									)
								)
							)
						)
						( add-before
							'build
							'set-cache
							( lambda
								_
								( setenv
									"XDG_CACHE_HOME"
									( getcwd)
								)
							)
						)
						( add-before
							'check
							'pre-check
							( lambda*
								(
									#:key
										inputs
									#:allow-other-keys
								)
								;; Tests require a running X server.
								( system "Xvfb :1 +extension GLX &")
								( setenv
									"DISPLAY"
									":1"
								)
								;; Tests write to $HOME.
								( setenv
									"HOME"
									( getcwd)
								)
								;; Tests look for those variables.
								( setenv
									"XDG_RUNTIME_DIR"
									( getcwd)
								)
								;; For missing '/etc/machine-id'.
								( setenv
									"DBUS_FATAL_WARNINGS"
									"0"
								)
								;; Required for the calendar test.
								( setenv
									"TZDIR"
									( search-input-directory
										inputs
										"share/zoneinfo"
									)
								)
							)
						)
						( add-after
							'install
							'move-files
							( lambda
								_
								( for-each
									mkdir-p
									( list
										( string-append
											#$output:bin
											"/share/applications"
										)
										( string-append
											#$output:bin
											"/share/icons"
										)
										( string-append
											#$output:bin
											"/share/man"
										)
										( string-append
											#$output:bin
											"/share/metainfo"
										)
										( string-append
											#$output:doc
											"/share/doc"
										)
									)
								)
								;; Move programs and related files to output 'bin'.
								( for-each
									( lambda
										( dir)
										( rename-file
											( string-append
												#$output
												dir
											)
											( string-append
												#$output:bin dir
											)
										)
									)
									( list
										"/share/applications"
										"/share/icons"
										"/share/man"
										"/share/metainfo"
									)
								)
								;; Move HTML documentation to output 'doc'.
								( rename-file
									( string-append
										#$output
										"/share/doc"
									)
									( string-append
										#$output:doc
										"/share/doc"
									)
								)
							)
						)
					)
				)
			)
			( native-inputs
				( list
					docbook-xml-4.3
					docbook-xsl
					gettext-minimal
					`( ,lucyGLib.glib "bin")
					lucyGI.gobject-introspection        ;for building introspection data
					; ( shinify graphene)
					; ( shinify gtk-doc)                      ;for building documentation
					gtk.graphene
					gtk.gtk-doc                      ;for building documentation
					gnuGLib.intltool
					libxslt                      ;for building man-pages
					pkg-config
					; ( notest
					; 	( shinify gnuGLib.python-pygobject)
					; )
					( noTests
						( replacePython gnuGLib.python-pygobject #:recursive #false)
					)
					;; These python modules are required for building documentation.
					python-docutils
					python-jinja2
					python-markdown
					python-markupsafe
					python-pygments
					python-toml
					python-typogrify
					sassc                        ;for building themes
					tzdata-for-tests
					; ( shinify vala)
					vala
					xorg-server-for-tests
					gi-docgen ; docs
				)
			)
			( inputs
				( list
					colord                       ;for color printing support
					cups                         ;for CUPS print-backend
					ffmpeg                       ;for ffmpeg media-backend
					fribidi
					; ( shinify gstreamer)                    ;for gstreamer media-backend
					; ( notest
					; 	( shinify gst-plugins-bad)
					; )              ;provides gstreamer-player
					; ( notest
					; 	( shinify gst-plugins-base)
					; )             ;provides gstreamer-gl
					; ( shinify harfbuzz)
					gstreamer                    ;for gstreamer media-backend
					gst-plugins-bad
					gst-plugins-base
					gtk.harfbuzz
					iso-codes
					; ( shinify json-glib)
					json-glib
					libcloudproviders            ;for cloud-providers support
					; ( shinify libgudev)                     ;for gstreamer-gl
					libgudev                     ;for gstreamer-gl
					libjpeg-turbo
					libpng
					libtiff
					python
					rest
					tracker                    ;for filechooser search support
				)
			)
			( propagated-inputs
				;; Following dependencies are referenced in .pc files.
				( list
					; ( shinify cairo)
					gtk.cairo
					fontconfig
					; ( shinify ( librsvg-for-system))
					( librsvg-for-system)
					lucyGLib.glib
					; ( shinify graphene)
					gtk.graphene
					libepoxy
					libx11                       ;for x11 display-backend
					libxcomposite
					libxcursor
					libxdamage
					libxext
					libxfixes
					libxi
					libxinerama                  ;for xinerama support
					libxkbcommon
					libxrandr
					libxrender
					; ( shinify pango)
					pango
					vulkan-headers
					vulkan-loader                ;for vulkan graphics API support
					; glslang ; vulkan too
					shaderc
					wayland                      ;for wayland display-backend
					wayland-protocols
				)
			)
			( native-search-paths
				( list
					( search-path-specification
						( variable "GUIX_GTK4_PATH")
						( files '( "lib/gtk-4.0"))
					)
				)
			)
			( search-paths native-search-paths)
			( home-page "https://www.gtk.org/")
			( synopsis "Cross-platform widget toolkit")
			( description
				"GTK is a multi-platform toolkit for creating graphical user
	interfaces.  Offering a complete set of widgets, GTK is suitable for projects
	ranging from small one-off tools to complete application suites."
			)
			( license license.lgpl2.1+)
		)
	)
)

( define
	gtk4-layer-shell
	( package
		; ( inherit gtk.gtk-layer-shell)
		( name "gtk4-layer-shell")
		( version "1.0.2")
		( home-page "https://github.com/wmww/gtk4-layer-shell")
		( synopsis "A library to create panels and other desktop components for Wayland using the Layer Shell protocol and GTK4")
		( description "A library for using the Layer Shell Wayland protocol with GTK4. With this library you can build desktop shell components such as panels, notifications and wallpapers. You can use it to anchor your windows to a corner or edge of the output, or stretch them across the entire output. This Library is compatible with C, C++ and any language that supports GObject introspection files (Python, Vala, etc).")
		( license license.expat)
		( source
			( origin
				( method git-fetch)
				( uri
					( git-reference
						( url "https://github.com/wmww/gtk4-layer-shell")
						( commit
							( string-append
								"v"
								version
							)
						)
					)
				)
				( file-name
					( git-file-name
						name
						version
					)
				)
				( sha256
					( base32 "1kppzbnw8sw0r0853arkgkgv0j84gg0cxa1w4bj2wqv484z27rvm")
				)
			)
		)
		( build-system meson-build-system)
		( arguments
			( list
				#:tests? #f ; disable for now due to broken 32
				#:configure-flags `( list
					"-Dtests=true"
					"-Dexamples=true"
				)
			)
		)
		( native-inputs
			( list
				pkg-config
				gnuGLib.gobject-introspection
				vala
			)
		)
		( inputs
			( list
				wayland
				gtk.gtk
			)
		)
	)
)
(define libadwaita
(package
(name "libadwaita")
(version "1.4.0")
(source (origin
(method url-fetch)
(uri (string-append "mirror://gnome/sources/libadwaita/"
(version-major+minor version) "/"
"libadwaita-" version ".tar.xz"))
(sha256
(base32
"1hj7kxza6263x662v4ffndlz8dhfx19cz3y4iwhnhdflaj50j6p5"))))
(build-system meson-build-system)
(arguments
`(
#:configure-flags ( list
	"-Dgtk_doc=true"
)
#:phases
(modify-phases %standard-phases
(add-before 'check 'pre-check
(lambda* (#:key inputs #:allow-other-keys)
;; Tests require a running X server.
(system "Xvfb :1 &")
(setenv "DISPLAY" ":1"))))))
(native-inputs
(list gettext-minimal
`(,lucyGLib.glib "bin")
lucyGI.gobject-introspection
gi-docgen
gtk.gtk-doc/stable
pkg-config
sassc
vala
xorg-server-for-tests))
(propagated-inputs
(list appstream gtk))                        ;libadwaita-1.pc 'Requires' it
(home-page "https://gnome.pages.gitlab.gnome.org/libadwaita/")
(synopsis "Building blocks for GNOME applications")
(description
"@code{libadwaita} offers widgets and objects to build GNOME
applications scaling from desktop workstations to mobile phones.  It is the
successor of @code{libhandy} for GTK4.")
(license license.lgpl2.1+)))

gtk
