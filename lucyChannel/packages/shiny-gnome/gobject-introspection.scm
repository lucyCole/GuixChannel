( define-module
	( lucyChannel packages shiny-gnome gobject-introspection)
	#:use-module ( lucyChannel packages shiny-gnome glib)

	#:use-module ( guix git-download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build utils)
	#:use-module ( gnu)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)

	#:use-module ( gnu packages python)
	#:use-module ( gnu packages pkg-config)
	#:use-module ( gnu packages bison)
	#:use-module ( gnu packages flex)
	#:use-module ( gnu packages libffi)
	#:use-module ( gnu packages compression)

	#:export (
		gobject-introspection
	)
)

; flex no version specified
; bison no version specified
; zlib no version specified
; meson changes based on libffi version, guix has best option

( define
	gobject-introspection
	( let
		(
			( revision "0")
			( commit "b8c3ff23b6c121433faca19ddf61fe8dc72a8930")
		)
		( package
			( name "gobject-introspection")
			( version
				( string-append
					"main"
					"-"
					revision
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://gitlab.gnome.org/GNOME/gobject-introspection.git")
						)
					)
					( sha256
						( base32 "0rb4yy65fl52bclblv6saq5pwm3r2rwszxxaz6nv7msyyrvbpas0")
					)
					( patches
						( search-patches
							"gobject-introspection-cc.patch"
							"gobject-introspection-girepository.patch"
							"gobject-introspection-absolute-shlib-path.patch"
						)
					)
				)
			)
			( build-system meson-build-system)
			( arguments
				`(
					,@( if
						( %current-target-system)
						`(
							#:configure-flags
							'(
								"-Dgi_cross_use_prebuilt_gi=true"
								;; Building introspection data requires running binaries
								;; for ‘host’ on ‘build’, so don't do that.
								;;
								;; TODO: it would be nice to have introspection data anyways
								;; as discussed here: https://issues.guix.gnu.org/50201#60.
								"-Dbuild_introspection_data=false"
							)
						)
						'()
					)
					#:phases ,#~( modify-phases
						%standard-phases
						#$@( if
							( %current-target-system)
							;; 'typelibs' is undefined.
							`(
								( add-after
									'unpack
									'set-typelibs
									( lambda
										_
										( substitute*
											"meson.build"
											(
												( "\\bsources: typelibs\\b")
												"sources: []"
											)
										)
									)
								)
							)
							'()
						)
						( add-after
							'unpack
							'do-not-use-/usr/bin/env
							( lambda
								_
								( substitute*
									"tools/g-ir-tool-template.in"
									(
										( "#!@PYTHON_CMD@")
										( string-append
											"#!"
											( which "python3")
										)
									)
								)
							)
						)
						#$@( if
							( %current-target-system)
							;; Meson gives python extensions an incorrect name, see
							;; <https://github.com/mesonbuild/meson/issues/7049>.
							#~(
								( add-after
									'install
									'rename-library
									#$( correct-library-name-phase
										( this-package-input "python")
										#~( string-append
											#$output
											"/lib/gobject-introspection/giscanner"
											"/_giscanner"
										)
									)
								)
							)
							#~()
						)
					)
				)
			)
			( native-inputs
				`(
					,@( if
						( %current-target-system)
						`( ( "python" ,python))
						'()
					)
					(
						"glib"
						,glib
						"bin"
					)
					( "pkg-config" ,pkg-config)
					( "bison" ,bison)
					( "flex" ,flex)
				)
			)
			( inputs
				`(
					("python" ,python)
					("zlib" ,zlib)
				)
			)
			( propagated-inputs
				( list
					glib
					;; In practice, GIR users will need libffi when using
					;; gobject-introspection.
					libffi
				)
			)
			( native-search-paths
				( list
					( search-path-specification
						( variable "GI_TYPELIB_PATH")
						( files '( "lib/girepository-1.0"))
					)
				)
			)
			( search-paths native-search-paths)
			( synopsis "GObject introspection tools and libraries")
			( description
				"GObject introspection is a middleware layer between
C libraries (using GObject) and language bindings.  The C library can be scanned
at compile time and generate metadata files, in addition to the actual native
C library.  Then language bindings can read this metadata and automatically
provide bindings to call into the C library."
			)
			( home-page "https://wiki.gnome.org/Projects/GObjectIntrospection")
			( license
				( list
					;; For library.
					license.lgpl2.0+
					;; For tools.
					license.gpl2+
				)
			)
			
		)
	)
)
