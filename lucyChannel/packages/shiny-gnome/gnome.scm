( define-module
	( lucyChannel packages shiny-gnome gnome)

	#:use-module ( guix git-download)
	#:use-module ( guix download)
	#:use-module ( guix gexp)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build utils)
	#:use-module ( guix utils)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)

	#:use-module (
		( gnu packages gnome)
		#:select (
			( vala . gnuVala)
		)
	)
	#:export (
		vala
	)
)

(define vala
(package
( inherit gnuVala)
(version "0.56.17")
(source (origin
(method url-fetch)
(uri (string-append "mirror://gnome/sources/vala/"
(version-major+minor version) "/"
"vala-" version ".tar.xz"))
(sha256
(base32
"0spd6ill4nnfpj13qm6700yqhrgmgkcl1wbmj9hrq17h9r70q416"))))))
vala
