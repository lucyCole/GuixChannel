( define-module
	( lucyChannel packages python-xyz)
	#:use-module( guix packages)
	#:use-module( guix gexp)
	#:use-module( guix download)
	#:use-module( guix build-system python)
	#:use-module( guix build-system pyproject)

	#:use-module( gnu packages gl)

	#:use-module(
		( guix licenses)
		#:prefix license.
	)
	#:export(
		python-glfw
	)
)
( define
	python-glfw
	( package
		( name "python-glfw")
		( version "2.6.3")
		( source
			( origin
				( method url-fetch)
				( uri
					( pypi-uri
						"glfw"
						version
					)
				)
				( sha256
					( base32 "00i9r7abaaxhd31zf9nllxk2d74lv254f8xw6ba6yc6glwdvm3fg")
				)
				( modules
					`(
						( guix build utils)
					)
				)
				( snippet
					#~( begin
						( substitute*
							"glfw/library.py"
							(
								( "        search_paths.extend\\(os\\.environ\\[path_environment_variable\\]\\.split\\(':'\\)\\)")
								( string-append
									"        search_paths.extend(os.environ[path_environment_variable].split(':'))\n"
									; "    search_paths+= [ os.environ[ \"GUIX_ENVIRONMENT\"]+ \"/lib\"]"
									"    search_paths+= [ \""
									#$glfw
									"\"+ \"/lib\",]"
								)
							)
						)
					)
				)
			)
		)
		( build-system pyproject-build-system)
		( propagated-inputs
			( list glfw)
		)
		( home-page "https://github.com/FlorianRhiem/pyGLFW")
		( synopsis "A ctypes-based wrapper for GLFW3.")
		( description "This package provides a ctypes-based wrapper for GLFW3.")
		( license license.expat)
	)
)
