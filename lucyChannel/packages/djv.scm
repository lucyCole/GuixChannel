( define-module
	( lucyChannel packages djv)
	#:use-module ( guix gexp)
	#:use-module ( guix git-download)
	#:use-module ( guix build-system cmake)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module ( guix packages)
	#:use-module ( guix build utils)
	#:use-module ( gnu packages gl)
	#:use-module ( gnu packages maths)
	#:use-module ( gnu packages python)
	#:use-module ( gnu packages web)
	#:use-module ( gnu packages linux)
	#:use-module ( gnu packages graphics)
	#:use-module ( gnu packages fontutils)
	#:use-module ( gnu packages video)
	#:use-module ( gnu packages image)
	#:use-module ( gnu packages image-processing)
	#:use-module ( gnu packages audio)
	#:use-module ( gnu packages compression)
	#:use-module ( ice-9 rdelim)
	#:export (
		djv
	)
)

( define
	fseq
	( let
		(
			( commit "cda02f1793fe7f58b1b76aa6d427a3190226769a")
			( revision "0")
		)
		( package
			( name "fseq")
			( version
				( git-version
					"main"
					revision
					commit
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://github.com/darbyjohnston/FSeq.git")
							( recursive? #f)
						)
					)
					( sha256
						( base32 "1n1c1f4ash249vdwxjmf7gp5bw1wm5w54i1mpms0a8xc3b3rhhqh")
					)
				)
			)
			( build-system cmake-build-system)
			( inputs
				( list
				)
			)
			( arguments
				`(
					#:configure-flags ( list
						"-DFSEQ_BUILD_TESTS=TRUE"
					)
				)
			)
			( home-page "https://github.com/darbyjohnston/FSeq")
			( synopsis "FSeq is a C library for working with sequences of files. ")
			( description
				"FSeq is a C library for working with sequences of files. File sequences are commonly used in the VFX and film industry to represent individual frames of an animation or film."
			)
			( license license.bsd-3)
		)
	)
)
( define
	djv
	( let
		(
			( commit "dcd46944b8caf79b70e79419f6b061fed3086e7c")
			( revision "0")
		)
		( package
			( name "djv")
			( version
				( git-version
					"2.0.8"
					revision
					commit
				)
			)
			( source
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( commit commit)
							( url "https://github.com/darbyjohnston/DJV.git")
							( recursive? #f)
						)
					)
					( sha256
						( base32 "0rdiv34qb4i5gms2bxhajl4p5c20sryhyzwq2b4n6vz05vy8ggnr")
					)
					( modules
						`(
							( guix build utils)
							( ice-9 rdelim)
						)
					)
					( snippet
						#~( begin
							; Glad setup

							; ( substitute*
							; 	"CMakeLists.txt"
							; 	(
							; 		( "\\$\\{CMAKE_SOURCE_DIR\\}/lib")
							; 		( string-append
							; 			"${CMAKE_SOURCE_DIR}/lib\n"
							; 			"${PROJECT_BINARY_DIR}/src/glad/include/glad\n"
							; 			"${PROJECT_BINARY_DIR}/src/glad/include/KHR"
							; 		)
							; 	)
							; 	(
							; 		( "    find_package\\(\\$\\{PACKAGE\\} REQUIRED\\)")
							; 		( string-append
							; 			"    if( ${PACKAGE} MATCHES glad)\n"
							; 			; Configure Glad to use static Khronos XML specifications
							; 			; instead of attempting to fetch them from the Internet.
							; 			"        option(GLAD_REPRODUCIBLE \"Reproducible build\" ON)\n"
							; 			; Use the CMake files from our glad package.
							; 			"        add_subdirectory("
							; 			#$glad
							; 			"/share/glad"
							; 			" src/glad)\n"
							; 			"    else()\n"
							; 			"        find_package(${PACKAGE} REQUIRED)\n"
							; 			"    endif()"
							; 		)
							; 	)
							; )

							; GL_*_KHR symbol is used in this file but our glad does not define these
							; At the time of writing, the value is the same for the non _KHR suffix-ed symbol
							( substitute*
								"lib/djvGL/GLFWSystem.cpp"
								(
									( "GL_.*_KHR" all)
									( string-drop-right
										all
										4
									)
								)
							)
							; I think this software expects a homo-geneous ilmbase
							; Instead it is divided, use the divided openexr
							( substitute*
								"cmake/Modules/FindOpenEXR.cmake"
								(
									( "find_package\\(IlmBase\\)")
									""
								)
								(
									( "\\$\\{IlmBase_INCLUDE_DIRS\\}")
									""
								)
								(
									( "Iex.h")
									"ImfArray.h"
								)
							)
						)
					)
				)
			)
			( build-system cmake-build-system)
			( native-inputs
				( list
					mesa
					glad
					fseq
					zlib
					glm
					glfw
					rapidjson
					alsa-lib
					rtaudio
					ilmbase
					libpng
					ijg-libjpeg
					libtiff
					openexr-2
					opencolorio
					freetype
					ffmpeg
					python
				)
			)
			( arguments
				( list

					; I am not doing this now
					; It can not create a "/Documents" directory
					; GLFW fails, perhaps Xvfb can be used
					; Wayland support is discussed here: "https://github.com/darbyjohnston/DJV/issues/6"
					; but is not seemingly implemented
					; maybe answers here: https://darbyjohnston.github.io/DJV/env_vars.html
					#:tests? #false

					#:modules '(
						( guix build cmake-build-system)
						( guix build utils)
					)

					#:phases #~( begin
						( modify-phases
							%standard-phases
							( add-after
								'install
								'installXDGComponent-s
								( lambda
									_
									( substitute*
										"../source/etc/Linux/djv.desktop.in"
										(
											( "@CPACK_PACKAGING_INSTALL_PREFIX@")
											#$output
										)
									)
									( let
										(
											(
												desktopDir
												( string-append
													#$output
													"/share/applications"
												)
											)
											(
												mimeDir
												( string-append
													#$output
													"/share/mime/application"
												)
											)
										)
										( mkdir-p desktopDir)
										( mkdir-p mimeDir)
										( copy-file
											"../source/etc/Linux/djv.desktop.in"
											( string-append
												desktopDir
												"/djv.desktop"
											)
										)
										( copy-file
											"../source/etc/Linux/djv.mime.xml"
											( string-append
												mimeDir
												"/djv.xml"
											)
										)
										( for-each
											( lambda
												( size)
												( define
													iconDir
													( string-append
														#$output
														"/share/icons/hicolor/"
														size
														"x"
														size
														"/apps"
													)
												)
												( mkdir-p  iconDir)
												( copy-file
													( string-append
														"../source/etc/Icons/djv-reel-"
														size
														".png"
													)
													( string-append
														iconDir
														"/djv.png"
													)
												)
											)
											( list
												"16"
												"32"
												"64"
												"128"
												"256"
												"512"
											)
										)
									)
								)
							)
						)
					)
					#:build-type "Release"
				)
			)
			( home-page "https://darbyjohnston.github.io/DJV/index.html")
			( synopsis "Feature rich viewing software for single images and image sequences")
			( description
				"DJV provides professional review software for VFX, animation, and film production. Playback high resolution, high bit-depth, image sequences and videos, with frame accurate control and color management. Available for Linux, Apple macOS, and Microsoft Windows. Source code is provided under a BSD style open source license."
			)
			( license license.bsd-3)
		)
	)
)
