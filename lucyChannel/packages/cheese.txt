c( define-module
	( lucyChannel packages python-xyz)
	#:use-module( guix packages)
	#:use-module( guix download)
	#:use-module ( guix build-system python)
	#:use-module ( guix build-system pyproject)
	#:use-module(
		( guix licenses)
		#:prefix license.
	)

	#:use-module( gnu packages python-xyz)
	#:use-module( gnu packages version-control)
	#:use-module( gnu packages glib)
	#:use-module( gnu packages rust-apps)
	#:use-module( gnu packages rust)
	#:use-module( gnu packages crates-io)
	#:use-module( gnu packages compression)

	#:export(
		python-ruff
		python-wsgiref
		python-frozen-flask
		python-flask-uploads
		python-flask-themes
		python-mimeparse
		python-mimerender
		python-flask-restless
		python-flask-pymongo
		python-flask-mail
		python-flask-gravatar
		python-flask-flatpages
		python-flask-cache
		python-visitor
		python-dominate
		python-flask-bootstrap
		python-flask-admin
		python-zip
		python-backend
		python-pygobject-stubs
	)
)
( define
	python-ruff
	( package
		( name "python-ruff")
		( version "0.1.6")
		( source
			( origin
				( method url-fetch)
				( uri
					( pypi-uri
						"ruff"
						version
					)
				)
				( sha256
					( base32 "113i42wx5g7c0lb2ywk3wgmjlds29dvg4zh9dgmdbsn62sdz428v")
				)
			)
		)
		( build-system pyproject-build-system)
		( inputs
			( list
				maturin
				rust
				rust-cargo-0.69
			)
		)
		( home-page "https://docs.astral.sh/ruff")
		( synopsis "An extremely fast Python linter and code formatter, written in Rust.")
		( description "An extremely fast Python linter and code formatter, written in Rust.")
		( license license.expat)
	)
)

( define
	python-wsgiref
  (package
    (name "python-wsgiref")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "wsgiref" version ".zip"))
       (sha256
        (base32 "0y8fyjmpq7vwwm4x732w97qbkw78rjwal5409k04cw4m03411rn7"))))
    (build-system pyproject-build-system)
    (native-inputs (list zip))
    (home-page "http://cheeseshop.python.org/pypi/wsgiref")
    (synopsis "WSGI (PEP 333) Reference Library")
    (description "WSGI (PEP 333) Reference Library")
    (license #f)))

( define
	python-frozen-flask
  (package
    (name "python-frozen-flask")
    (version "1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Frozen-Flask" version))
       (sha256
        (base32 "1pcar5ds15qd3cq4480n34mhfw0ly34zkjv0qn62ljwaig703sl6"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask))
    (native-inputs (list python-pytest))
    (home-page "")
    (synopsis "Freezes a Flask application into a set of static files.")
    (description "Freezes a Flask application into a set of static files.")
    (license #f)))

( define
	python-flask-uploads
  (package
    (name "python-flask-uploads")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Uploads" version))
       (sha256
        (base32 "00nb3s29vkqhb0196llyq1bgrirkmaxxwfmn0ap50zb66dhbvv2k"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask))
    (home-page "https://github.com/maxcountryman/flask-uploads")
    (synopsis "Flexible and efficient upload handling for Flask")
    (description "Flexible and efficient upload handling for Flask")
    (license license.expat)))

( define
	python-flask-themes
  (package
    (name "python-flask-themes")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Themes" version))
       (sha256
        (base32 "1z6r12z4gdxlnxd3sj99v4vnl3757wm6b4pwxwvsmksckb9gv1h5"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask))
    (home-page "http://bitbucket.org/leafstorm/flask-themes/")
    (synopsis "Provides infrastructure for theming Flask applications")
    (description
     "This package provides infrastructure for theming Flask applications")
    (license license.expat)))

( define
	python-mimeparse
  (package
    (name "python-mimeparse")
    (version "1.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "python-mimeparse" version))
       (sha256
        (base32 "0y2g6cl660bpz11srgqyvvm8gmywpgyx8g0xfvbiyr0af0yv1r3n"))))
    (build-system pyproject-build-system)
    (home-page "https://github.com/dbtsai/python-mimeparse")
    (synopsis
     "A module provides basic functions for parsing mime-type names and matching them against a list of media-ranges.")
    (description
     "This package provides a module provides basic functions for parsing mime-type
names and matching them against a list of media-ranges.")
    (license #f)))

( define
	python-mimerender
  (package
    (name "python-mimerender")
    (version "0.6.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "mimerender" version))
       (sha256
        (base32 "1imim78dypbl9fvrz21j8f13q8i96dx90m7f5ib3z371zrz3gwg7"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-python-mimeparse))
    (home-page "https://github.com/martinblech/mimerender")
    (synopsis
     "RESTful HTTP Content Negotiation for Flask, Bottle, web.py and webapp2 (Google App Engine)")
    (description
     "RESTful HTTP Content Negotiation for Flask, Bottle, web.py and webapp2 (Google
App Engine)")
    (license license.expat)))

( define
	python-flask-restless
  (package
    (name "python-flask-restless")
    (version "0.17.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Restless" version))
       (sha256
        (base32 "1dn2g3qkgvbbs4165hng82gkplm1bnxf010qkaf26ixx1bl7zr0x"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask python-mimerender python-dateutil
                             python-sqlalchemy))
    (home-page "http://github.com/jfinkels/flask-restless")
    (synopsis "A Flask extension for easy ReSTful API generation")
    (description
     "This package provides a Flask extension for easy @code{ReSTful} API generation")
    (license #f)))

( define
	python-flask-pymongo
  (package
    (name "python-flask-pymongo")
    (version "2.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-PyMongo" version))
       (sha256
        (base32 "051kwdk07y4xm4yawcjhn6bz8swxp9nanv7jj35mz2l0r0nv03k2"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask python-pymongo))
    (home-page "http://flask-pymongo.readthedocs.org/")
    (synopsis "PyMongo support for Flask applications")
    (description "@code{PyMongo} support for Flask applications")
    (license license.bsd-3)))

( define
	python-flask-mail
  (package
    (name "python-flask-mail")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Mail" version))
       (sha256
        (base32 "0hazjc351s3gfbhk975j8k65cg4gf31yq404yfy0gx0bjjdfpr92"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-blinker python-flask))
    (home-page "https://github.com/rduplain/flask-mail")
    (synopsis "Flask extension for sending email")
    (description "Flask extension for sending email")
    (license license.bsd-3)))

( define
	python-flask-gravatar
  (package
    (name "python-flask-gravatar")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Gravatar" version))
       (sha256
        (base32 "1qb2ylirjajdqsmldhwfdhf8i86k7vlh3y4gnqfqj4n6q8qmyrk0"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask))
    (native-inputs (list python-check-manifest
                         python-coverage
                         python-isort
                         python-pydocstyle
                         python-pytest
                         python-pytest-cache
                         python-pytest-cov
                         python-pytest-pep8))
    (home-page "https://github.com/zzzsochi/Flask-Gravatar/")
    (synopsis
     "Small extension for Flask to make usage of Gravatar service easy.")
    (description
     "Small extension for Flask to make usage of Gravatar service easy.")
    (license license.bsd-3)))

( define
	python-flask-flatpages
  (package
    (name "python-flask-flatpages")
    (version "0.8.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-FlatPages" version))
       (sha256
        (base32 "0sdws2y801a00c3gic86ybqwbvqqysmw6anhkd44b39h99bhqgs0"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask
                             python-jinja2
                             python-markdown
                             python-pytz
                             python-pyyaml
                             python-six))
    (home-page "")
    (synopsis "Provides flat static pages to a Flask application")
    (description
     "This package provides flat static pages to a Flask application")
    (license #f)))

( define
	python-flask-cache
  (package
    (name "python-flask-cache")
    (version "0.13.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Cache" version))
       (sha256
        (base32 "1x3ygki1x5rp3msxaxzx8n7frd5j71fyjxp2ivpm8f06pjlnq4lh"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask))
    (home-page "http://github.com/thadeusb/flask-cache")
    (synopsis "Adds cache support to your Flask application")
    (description "Adds cache support to your Flask application")
    (license license.bsd-3)))

( define
	python-visitor
  (package
    (name "python-visitor")
    (version "0.1.3")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "visitor" version))
       (sha256
        (base32 "02j87v93c50gz68gbgclmbqjcwcr7g7zgvk7c6y4x1mnn81pjwrc"))))
    (build-system pyproject-build-system)
    (home-page "http://github.com/mbr/visitor")
    (synopsis "A tiny pythonic visitor implementation.")
    (description
     "This package provides a tiny pythonic visitor implementation.")
    (license license.expat)))

( define
	python-dominate
  (package
    (name "python-dominate")
    (version "2.9.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "dominate" version))
       (sha256
        (base32 "1zbjszd6jsk8p0a21rclz5gzjbsxwimdf0hp79a1h8j3xbmr2mxi"))))
    (build-system pyproject-build-system)
    (home-page "")
    (synopsis
     "Dominate is a Python library for creating and manipulating HTML documents using an elegant DOM API.")
    (description
     "Dominate is a Python library for creating and manipulating HTML documents using
an elegant DOM API.")
    (license #f)))

( define
	python-flask-bootstrap
  (package
    (name "python-flask-bootstrap")
    (version "3.3.7.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Bootstrap" version))
       (sha256
        (base32 "1j1s2bplaifsnmr8vfxa3czca4rz78xyhrg4chx39xl306afs26b"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-dominate python-flask python-visitor))
    (home-page "http://github.com/mbr/flask-bootstrap")
    (synopsis
     "An extension that includes Bootstrap in your project, without any boilerplate code.")
    (description
     "An extension that includes Bootstrap in your project, without any boilerplate
code.")
    (license license.bsd-3)))

( define
	python-flask-admin
  (package
    (name "python-flask-admin")
    (version "1.6.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Admin" version))
       (sha256
        (base32 "0s9k4dw1n96bv1lvh9m4fmn1sv165ps3bp6p04d62sibhfpy5ji4"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-flask python-wtforms))
    (home-page "https://github.com/flask-admin/flask-admin/")
    (synopsis "Simple and extensible admin interface framework for Flask")
    (description "Simple and extensible admin interface framework for Flask")
    (license license.bsd-3)))

( define
	python-zip
  (package
    (name "python-zip")
    (version "0.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "zip" version))
       (sha256
        (base32 "18agz94zz086z023y5kmchizg2wmwmmjdj14g8hyjqb777i7j2qd"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-blinker
                             python-bumpversion
                             python-click
                             python-colorama
                             python-coverage
                             python-cryptography
                             python-flake8
                             python-flask
                             python-flask-admin
                             python-flask-bootstrap
                             python-flask-cache
                             python-flask-flatpages
                             python-flask-gravatar
                             python-flask-login
                             python-flask-mail
                             python-flask-pymongo
                             python-flask-restless
                             python-flask-sqlalchemy
                             python-flask-themes
                             python-flask-uploads
                             python-flask-wtf
                             python-frozen-flask
                             python-jinja2
                             python-markdown
                             python-networkx
                             python-pymongo
                             python-pytest
                             python-dateutil
                             python-pyyaml
                             python-six
                             python-sphinx
                             python-sqlalchemy
                             python-tox
                             python-watchdog
                             python-werkzeug
                             python-wheel
                             python-wsgiref
                             python-wtforms))
    (home-page "https://github.com/kdheepak/zip")
    (synopsis "")
    (description "")
    (license #f)))

( define
	python-backend
  (package
    (name "python-backend")
    (version "0.2.4.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "backend" version ".zip"))
       (sha256
        (base32 "17p70swiawv0dzmcd04szdaxsfi5dwnfjqjkbcppacf7fkmghqg1"))))
    (build-system pyproject-build-system)
    (native-inputs (list zip unzip))
    (home-page "https://github.com/tr4n2uil/backend.django")
    (synopsis "Backend Utility Tools")
    (description "Backend Utility Tools")
    (license license.expat)))

( define
	python-pygobject-stubs
	( package
		( name "python-pygobject-stubs")
		( version "2.10.0")
		( source
			( origin
				( method url-fetch)
				( uri
					( pypi-uri
						"PyGObject-stubs"
						version
					)
				)
				( sha256
					( base32 "087yg7p163azj1dsmr8f3fnhs1lvha6011v06wh3b1wipfdjz161")
				)
			)
		)
		( build-system pyproject-build-system)
		( native-inputs
			( list
				python-black
				python-codespell
				python-isort
				python-pre-commit
				python-pygobject
				python-backend
				; python-ruff
			)
		)
		( home-page "")
		( synopsis "Typing stubs for PyGObject")
		( description "Typing stubs for @code{PyGObject}")
		( license #f)
	)
)
python-pygobject-stubs
