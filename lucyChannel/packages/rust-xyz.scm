( define-module
	( lucyChannel packages rust-xyz)
	#:use-module ( guix packages)
	#:use-module ( guix download)

	#:use-module ( gnu packages crates-gtk)
	#:use-module ( gnu packages crates-graphics)
	; #:use-module ;non rust
	#:use-module ( gnu packages glib)
	#:use-module ( gnu packages tls)
	#:use-module ( gnu packages gnome)
	#:use-module ( gnu packages gtk)
	#:use-module ( gnu packages shells)
	#:use-module ( gnu packages pkg-config)

	#:use-module ( gnu packages crates-io)
	#:use-module ( gnu packages crates-windows)
	#:use-module ( gnu packages crates-web)
	#:use-module ( guix build-system cargo)

	#:use-module ( gnu packages fonts)
	#:use-module ( gnu packages freedesktop)
	#:use-module ( gnu packages xdisorg)

	#:use-module (
		( guix licenses)
		#:prefix license:
	)
)
( define
	unknown-license!
	( license:non-copyleft "https://urmumshouse.com")
)
;;; END


(define-public rust-tracker-macros-0.2
  (package
    (name "rust-tracker-macros")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracker-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ps5n0iscj8b3rj04yf97brgxmsryxxyfp90k0rdmvz0zd39f0na"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-2))))
    (home-page "https://github.com/AaronErhardt/Tracker")
    (synopsis "Macros for the tracker crate")
    (description "Macros for the tracker crate")
    (license (list license:asl2.0 license:expat))))

(define-public rust-tracker-0.2
  (package
    (name "rust-tracker")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracker" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1y3bgw08n87ssxs1gfn0ifcj2hmz5vk9rdsmpzv8f09pbv8kd5pz"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-tracker-macros" ,rust-tracker-macros-0.2))))
    (home-page "https://github.com/AaronErhardt/Tracker")
    (synopsis "A macro to track changes on structs")
    (description "This package provides a macro to track changes on structs")
    (license (list license:asl2.0 license:expat))))

(define-public rust-relm4-macros-0.5
  (package
    (name "rust-relm4-macros")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "relm4-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "09m34w6mjasnx6ia8chw7zhl1xxgl2rxssni2yxj8raa8cg1km14"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib gtk openssl))
    (home-page "https://relm4.org")
    (synopsis "An idiomatic GUI library inspired by Elm and based on gtk4-rs")
    (description
     "An idiomatic GUI library inspired by Elm and based on gtk4-rs")
    (license (list license:asl2.0 license:expat))))

(define-public rust-libpanel-sys-0.1
  (package
    (name "rust-libpanel-sys")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libpanel-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0xjp47ydqvcpz6vwywqlmngd01dpgmx1wdwlsydwiylqwv86y92p"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gdk4-sys" ,rust-gdk4-sys-0.5)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-gtk4-sys" ,rust-gtk4-sys-0.5)
                       ("rust-libadwaita-sys" ,rust-libadwaita-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib gtk))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libpanel-rs/")
    (synopsis "FFI bindings for GNOME libpanel")
    (description "FFI bindings for GNOME libpanel")
    (license license:expat)))

(define-public rust-libpanel-0.1
  (package
    (name "rust-libpanel")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libpanel" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1v3zs9c3678njwc9yb4am09kz913mdhc6hysf38323rk7hlc67lc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-gdk4" ,rust-gdk4-0.5)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-gtk4" ,rust-gtk4-0.5)
                       ("rust-libadwaita" ,rust-libadwaita-0.2)
                       ("rust-libpanel-sys" ,rust-libpanel-sys-0.1)
                       ("rust-once-cell" ,rust-once-cell-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib gtk))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libpanel-rs/")
    (synopsis "Rust bindings for GNOME libpanel")
    (description "Rust bindings for GNOME libpanel")
    (license license:expat)))

(define-public rust-libadwaita-sys-0.2
  (package
    (name "rust-libadwaita-sys")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libadwaita-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0nsxv5gk8k9p40cdmxn856p76yxkczarvzfph404li9b6y12k46y"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gdk4-sys" ,rust-gdk4-sys-0.5)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-gtk4-sys" ,rust-gtk4-sys-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.16)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list libadwaita))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/")
    (synopsis "FFI bindings for libadwaita")
    (description "FFI bindings for libadwaita")
    (license license:expat)))

(define-public rust-libadwaita-0.2
  (package
    (name "rust-libadwaita")
    (version "0.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "libadwaita" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0j3c5c4gvy06w8a3iv8ilin2j9jr7hkqqrmz3ik4ywpishi0gylx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.16)
                       ("rust-gdk4" ,rust-gdk4-0.5)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-gtk4" ,rust-gtk4-0.5)
                       ("rust-libadwaita-sys" ,rust-libadwaita-sys-0.2)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pango" ,rust-pango-0.16))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list libadwaita))
    (home-page "https://world.pages.gitlab.gnome.org/Rust/libadwaita-rs/")
    (synopsis "Rust bindings for libadwaita")
    (description "Rust bindings for libadwaita")
    (license license:expat)))

(define-public rust-gtk4-sys-0.5
  (package
    (name "rust-gtk4-sys")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gtk4-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "018nnwas5acqwp73r7c19ssgx2a6d9mkdxwrzk7zgkys7x75cw73"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.16)
                       ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.16)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.5)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.16)
                       ("rust-gsk4-sys" ,rust-gsk4-sys-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.16)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings of GTK 4")
    (description "FFI bindings of GTK 4")
    (license license:expat)))

(define-public rust-gtk4-macros-0.5
  (package
    (name "rust-gtk4-macros")
    (version "0.5.6")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gtk4-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "194mngqkl0apajk10bkba1r45jsfrggm5fc05lsrp9ln2di9v0j2"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
                       ("rust-proc-macro-error" ,rust-proc-macro-error-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quick-xml" ,rust-quick-xml-0.27)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "Macros helpers for GTK 4 bindings")
    (description "Macros helpers for GTK 4 bindings")
    (license license:expat)))

(define-public rust-gsk4-sys-0.5
  (package
    (name "rust-gsk4-sys")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gsk4-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1iafj10g727d1kib70y65jx2h17xxh6swfzbqfc9ylj2pvq66nhr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.16)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.5)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.16)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings of GSK 4")
    (description "FFI bindings of GSK 4")
    (license license:expat)))

(define-public rust-gsk4-0.5
  (package
    (name "rust-gsk4")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gsk4" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "14glfi5skr3jsv219cg9rs0d2313gz2cjjhj4ar07a1cqpskj4jr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.16)
                       ("rust-gdk4" ,rust-gdk4-0.5)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-graphene-rs" ,rust-graphene-rs-0.16)
                       ("rust-gsk4-sys" ,rust-gsk4-sys-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango" ,rust-pango-0.16))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GSK 4 library")
    (description "Rust bindings of the GSK 4 library")
    (license license:expat)))

(define-public rust-graphene-sys-0.16
  (package
    (name "rust-graphene-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "graphene-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1mnwwvdqvbgi2fvaljgp4xpvwirzq83ycwgazpmb8wirglrq5amr"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list graphene))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libgraphene-1.0")
    (description "FFI bindings to libgraphene-1.0")
    (license license:expat)))

(define-public rust-graphene-rs-0.16
  (package
    (name "rust-graphene-rs")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "graphene-rs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0ys40hfx6fmc78mbjsx0advw1vm8fjddiprvvwh9il768z9v9v4m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib" ,rust-glib-0.16)
                       ("rust-graphene-sys" ,rust-graphene-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list graphene))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the Graphene library")
    (description "Rust bindings for the Graphene library")
    (license license:expat)))

(define-public rust-pango-0.16
  (package
    (name "rust-pango")
    (version "0.16.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pango" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "150y1chgf6dy40w00dnbdh5kpq2r80c5d1h2knw3f446f6r6dzyd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pango-sys" ,rust-pango-sys-0.16))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list pango))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the Pango library")
    (description "Rust bindings for the Pango library")
    (license license:expat)))

(define-public rust-pango-sys-0.16
  (package
    (name "rust-pango-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pango-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zq4iw6x56qmhdabil2lz56mx737jnm2h6f35i6y14x2m44lj4wy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list pango))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libpango-1.0")
    (description "FFI bindings to libpango-1.0")
    (license license:expat)))

(define-public rust-gdk4-sys-0.5
  (package
    (name "rust-gdk4-sys")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gdk4-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1nwify5vin580635vn0i16dmnrj4d2h7y5rl6lmzw0998d4wnmfy"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.16)
                       ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.16)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango-sys" ,rust-pango-sys-0.16)
                       ("rust-pkg-config" ,rust-pkg-config-0.3)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings of GDK 4")
    (description "FFI bindings of GDK 4")
    (license license:expat)))

(define-public rust-gdk4-0.5
  (package
    (name "rust-gdk4")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gdk4" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1s811n7a47gbv8gjwn98848r5p3sgy3xdzm7iqghk7dz1qrq28dv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.16)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.16)
                       ("rust-gdk4-sys" ,rust-gdk4-sys-0.5)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pango" ,rust-pango-0.16))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gtk))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GDK 4 library")
    (description "Rust bindings of the GDK 4 library")
    (license license:expat)))

(define-public rust-gio-0.16
  (package
    (name "rust-gio")
    (version "0.16.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gio" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "14nh7m8qpdswzhjfhwhl24rmijd9ybpwdx8f2qlhlaaaafs8871a"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-io" ,rust-futures-io-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pin-project-lite" ,rust-pin-project-lite-0.2)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the Gio library")
    (description "Rust bindings for the Gio library")
    (license license:expat)))

(define-public rust-gdk-pixbuf-sys-0.16
  (package
    (name "rust-gdk-pixbuf-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gdk-pixbuf-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05h0sz3jkia38qpz7s8viygqwnwamvcp1053kr3i04jzg9wwz4ih"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gdk-pixbuf))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libgdk_pixbuf-2.0")
    (description "FFI bindings to libgdk_pixbuf-2.0")
    (license license:expat)))

(define-public rust-gdk-pixbuf-0.16
  (package
    (name "rust-gdk-pixbuf")
    (version "0.16.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gdk-pixbuf" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "01csvn2j60jms2c29qb06fwc2d8gnj6dhgjrhsnjkl79vrh8qmy3"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-gdk-pixbuf-sys" ,rust-gdk-pixbuf-sys-0.16)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-libc" ,rust-libc-0.2))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list gdk-pixbuf))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the GdkPixbuf library")
    (description "Rust bindings for the @code{GdkPixbuf} library")
    (license license:expat)))

(define-public rust-glib-macros-0.16
  (package
    (name "rust-glib-macros")
    (version "0.16.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "glib-macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0yx6c1sjjajhvvkpjzz64x4gqlcx3dhkgspzjqg6z93shhjr66pv"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-anyhow" ,rust-anyhow-1)
                       ("rust-heck" ,rust-heck-0.4)
                       ("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
                       ("rust-proc-macro-error" ,rust-proc-macro-error-1)
                       ("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the GLib library, proc macros crate")
    (description "Rust bindings for the GLib library, proc macros crate")
    (license license:expat)))

(define-public rust-gobject-sys-0.16
  (package
    (name "rust-gobject-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gobject-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1wd0c2vrp9xi7x0mcsd88s0iq8qizi74vcpvyb3i4amf0yfbn81m"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libgobject-2.0")
    (description "FFI bindings to libgobject-2.0")
    (license license:expat)))

(define-public rust-gio-sys-0.16
  (package
    (name "rust-gio-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gio-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0aaj1cfcb8wb22ijyjwgnkqb2jbkn2kmihkzajajl14xwfw97dp9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6)
                       ("rust-winapi" ,rust-winapi-0.3))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libgio-2.0")
    (description "FFI bindings to libgio-2.0")
    (license license:expat)))

(define-public rust-glib-0.16
  (package
    (name "rust-glib")
    (version "0.16.9")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "glib" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1fkf9l0b46nwma7gb7g6p4d9wgssmzr5zjrj52ixbgnyr5sj9ahn"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-futures-core" ,rust-futures-core-0.3)
                       ("rust-futures-executor" ,rust-futures-executor-0.3)
                       ("rust-futures-task" ,rust-futures-task-0.3)
                       ("rust-futures-util" ,rust-futures-util-0.3)
                       ("rust-gio-sys" ,rust-gio-sys-0.16)
                       ("rust-glib-macros" ,rust-glib-macros-0.16)
                       ("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-gobject-sys" ,rust-gobject-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the GLib library")
    (description "Rust bindings for the GLib library")
    (license license:expat)))

(define-public rust-freetype-sys-0.16
  (package
    (name "rust-freetype-sys")
    (version "0.16.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "freetype-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1clijnjv0gl4a3a1cnrlx3276jk07gp3wlgkhp2wclbxmmp6jz51"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-cmake" ,rust-cmake-0.1)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "https://github.com/PistonDevelopers/freetype-sys")
    (synopsis "Low level binding for FreeType font library")
    (description "Low level binding for @code{FreeType} font library")
    (license license:expat)))

(define-public rust-freetype-rs-0.31
  (package
    (name "rust-freetype-rs")
    (version "0.31.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "freetype-rs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1fk01dza1i6lzripvg931xgdm1lzgxsc364hzynbbm16vb3da022"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-freetype-sys" ,rust-freetype-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/PistonDevelopers/freetype-rs")
    (synopsis "Bindings for FreeType font library")
    (description "Bindings for @code{FreeType} font library")
    (license license:expat)))

(define-public rust-glib-sys-0.16
  (package
    (name "rust-glib-sys")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "glib-sys" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0r9anrbz7cfck75dkcixivjqnk2jy3v2vhks7aivy1kd6534y6n6"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libglib-2.0")
    (description "FFI bindings to libglib-2.0")
    (license license:expat)))

(define-public rust-cairo-sys-rs-0.16
  (package
    (name "rust-cairo-sys-rs")
    (version "0.16.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "cairo-sys-rs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "08gljr8qixiswdr7vkj0y19ih1psdqrf2y41cnlwzggs0npz8j3w"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-glib-sys" ,rust-glib-sys-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-system-deps" ,rust-system-deps-6)
                       ("rust-winapi" ,rust-winapi-0.3)
                       ("rust-x11" ,rust-x11-2))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list cairo))
    (home-page "https://gtk-rs.org/")
    (synopsis "FFI bindings to libcairo")
    (description "FFI bindings to libcairo")
    (license license:expat)))

(define-public rust-cairo-rs-0.16
  (package
    (name "rust-cairo-rs")
    (version "0.16.7")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "cairo-rs" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0zf9kw9k5ia6r8aba712q1n5ysh16ih6qivgiwilrf18xhamn4pk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-sys-rs" ,rust-cairo-sys-rs-0.16)
                       ("rust-freetype-rs" ,rust-freetype-rs-0.31)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list cairo))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings for the Cairo library")
    (description "Rust bindings for the Cairo library")
    (license license:expat)))

(define-public rust-gtk4-0.5
  (package
    (name "rust-gtk4")
    (version "0.5.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "gtk4" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0pl5c4b45giy634zd01zv3ipc5dhvbrx7zf47liklj7gbnkdp2gx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-bitflags" ,rust-bitflags-1)
                       ("rust-cairo-rs" ,rust-cairo-rs-0.16)
                       ("rust-field-offset" ,rust-field-offset-0.3)
                       ("rust-futures-channel" ,rust-futures-channel-0.3)
                       ("rust-gdk-pixbuf" ,rust-gdk-pixbuf-0.16)
                       ("rust-gdk4" ,rust-gdk4-0.5)
                       ("rust-gio" ,rust-gio-0.16)
                       ("rust-glib" ,rust-glib-0.16)
                       ("rust-graphene-rs" ,rust-graphene-rs-0.16)
                       ("rust-gsk4" ,rust-gsk4-0.5)
                       ("rust-gtk4-macros" ,rust-gtk4-macros-0.5)
                       ("rust-gtk4-sys" ,rust-gtk4-sys-0.5)
                       ("rust-libc" ,rust-libc-0.2)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-pango" ,rust-pango-0.16))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib gtk pango))
    (home-page "https://gtk-rs.org/")
    (synopsis "Rust bindings of the GTK 4 library")
    (description "Rust bindings of the GTK 4 library")
    (license license:expat)))

(define-public rust-fragile-2
  (package
    (name "rust-fragile")
    (version "2.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "fragile" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1ajfdnwdn921bhjlzyvsqvdgci8ab40ln6w9ly422lf8svb428bc"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-slab" ,rust-slab-0.4))))
    (home-page "https://github.com/mitsuhiko/fragile")
    (synopsis
     "Provides wrapper types for sending non-send values to other threads.")
    (description
     "This package provides wrapper types for sending non-send values to other
threads.")
    (license license:asl2.0)))

(define-public rust-relm4-0.5
  (package
    (name "rust-relm4")
    (version "0.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "relm4" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1gwgzf50x11blp506jwp87sh82wj90nynvixxzyxlfxl6349i8h1"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-async-trait" ,rust-async-trait-0.1)
                       ("rust-flume" ,rust-flume-0.10)
                       ("rust-fragile" ,rust-fragile-2)
                       ("rust-futures" ,rust-futures-0.3)
                       ("rust-gtk4" ,rust-gtk4-0.5)
                       ("rust-libadwaita" ,rust-libadwaita-0.2)
                       ("rust-libpanel" ,rust-libpanel-0.1)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-relm4-macros" ,rust-relm4-macros-0.5)
                       ("rust-tokio" ,rust-tokio-1)
                       ("rust-tracing" ,rust-tracing-0.1))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.4)
                                   ("rust-futures" ,rust-futures-0.3)
                                   ("rust-rand" ,rust-rand-0.8)
                                   ("rust-reqwest" ,rust-reqwest-0.11)
                                   ("rust-tokio" ,rust-tokio-1)
                                   ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3)
                                   ("rust-tracker" ,rust-tracker-0.2))))
    (native-inputs
     (list pkg-config))
    (inputs
     (list glib gtk openssl))
    (home-page "https://relm4.org")
    (synopsis "An idiomatic GUI library inspired by Elm and based on gtk4-rs")
    (description
     "An idiomatic GUI library inspired by Elm and based on gtk4-rs")
    (license (list license:asl2.0 license:expat))))


;;;;; END




(define-public rust-lru-0.7
  (package
    (name "rust-lru")
    (version "0.7.8")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lru" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0yp4ai5rpr2czxklzxxx98p6l2aqv4g1906j3dr4b0vfgfxbx6g9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.12))))
    (home-page "https://github.com/jeromefroe/lru-rs")
    (synopsis "A LRU cache implementation")
    (description "This package provides a LRU cache implementation")
    (license license:expat)))

(define-public rust-tracing-log-0.2
  (package
    (name "rust-tracing-log")
    (version "0.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracing-log" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1hs77z026k730ij1a9dhahzrl0s073gfa2hm5p0fbl0b80gmz1gf"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-ahash" ,rust-ahash-0.7)
                       ("rust-log" ,rust-log-0.4)
                       ("rust-lru" ,rust-lru-0.7)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-tracing-core" ,rust-tracing-core-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Provides compatibility between `tracing` and the `log` crate.
")
    (description
     "This package provides compatibility between `tracing` and the `log` crate.")
    (license license:expat)))

(define-public rust-pure-rust-locales-0.7
  (package
    (name "rust-pure-rust-locales")
    (version "0.7.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pure-rust-locales" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0cl46srhxzj0jlvfp73l8l9qw54qwa04zywaxdf73hidwqlsh0pd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/cecton/pure-rust-locales")
    (synopsis
     "Pure Rust locales imported directly from the GNU C Library. `LC_COLLATE` and `LC_CTYPE` are not yet supported.")
    (description
     "Pure Rust locales imported directly from the GNU C Library. `LC_COLLATE` and
`LC_CTYPE` are not yet supported.")
    (license (list license:expat license:asl2.0))))

(define-public rust-android-tzdata-0.1
  (package
    (name "rust-android-tzdata")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "android-tzdata" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1w7ynjxrfs97xg3qlcdns4kgfpwcdv824g611fq32cag4cdr96g9"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/RumovZ/android-tzdata")
    (synopsis "Parser for the Android-specific tzdata file")
    (description "Parser for the Android-specific tzdata file")
    (license (list license:expat license:asl2.0))))

(define-public rust-chrono-0.4
  (package
    (name "rust-chrono")
    (version "0.4.31")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "chrono" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0f6vg67pipm8cziad2yms6a639pssnvysk1m05dd9crymmdnhb3z"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-android-tzdata" ,rust-android-tzdata-0.1)
                       ("rust-arbitrary" ,rust-arbitrary-1)
                       ("rust-iana-time-zone" ,rust-iana-time-zone-0.1)
                       ("rust-js-sys" ,rust-js-sys-0.3)
                       ("rust-num-traits" ,rust-num-traits-0.2)
                       ("rust-pure-rust-locales" ,rust-pure-rust-locales-0.7)
                       ("rust-rkyv" ,rust-rkyv-0.7)
                       ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-wasm-bindgen" ,rust-wasm-bindgen-0.2)
                       ("rust-windows-targets" ,rust-windows-targets-0.48))))
    (home-page "https://github.com/chronotope/chrono")
    (synopsis "Date and time library for Rust")
    (description "Date and time library for Rust")
    (license (list license:expat license:asl2.0))))

(define-public rust-tracing-subscriber-0.3
  (package
    (name "rust-tracing-subscriber")
    (version "0.3.18")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracing-subscriber" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "12vs1bwk4kig1l2qqjbbn2nm5amwiqmkcmnznylzmnfvjy6083xd"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-matchers" ,rust-matchers-0.1)
                       ("rust-nu-ansi-term" ,rust-nu-ansi-term-0.46)
                       ("rust-once-cell" ,rust-once-cell-1)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-regex" ,rust-regex-1)
                       ("rust-serde" ,rust-serde-1)
                       ("rust-serde-json" ,rust-serde-json-1)
                       ("rust-sharded-slab" ,rust-sharded-slab-0.1)
                       ("rust-smallvec" ,rust-smallvec-1)
                       ("rust-thread-local" ,rust-thread-local-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing" ,rust-tracing-0.1)
                       ("rust-tracing-core" ,rust-tracing-core-0.1)
                       ("rust-tracing-log" ,rust-tracing-log-0.2)
                       ("rust-tracing-serde" ,rust-tracing-serde-0.1)
                       ("rust-valuable" ,rust-valuable-0.1)
                       ("rust-valuable-serde" ,rust-valuable-serde-0.1))))
    (home-page "https://tokio.rs")
    (synopsis "Utilities for implementing and composing `tracing` subscribers.
")
    (description
     "Utilities for implementing and composing `tracing` subscribers.")
    (license license:expat)))

(define-public rust-tracing-appender-0.2
  (package
    (name "rust-tracing-appender")
    (version "0.2.3")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "tracing-appender" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1kq69qyjvb4dxch5c9zgii6cqhy9nkk81z0r4pj3y2nc537fhrim"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-crossbeam-channel" ,rust-crossbeam-channel-0.5)
                       ("rust-parking-lot" ,rust-parking-lot-0.12)
                       ("rust-thiserror" ,rust-thiserror-1)
                       ("rust-time" ,rust-time-0.3)
                       ("rust-tracing-subscriber" ,rust-tracing-subscriber-0.3))
       #:cargo-development-inputs (("rust-criterion" ,rust-criterion-0.3)
                                   ("rust-tempfile" ,rust-tempfile-3)
                                   ("rust-tracing" ,rust-tracing-0.1))))
    (home-page "https://tokio.rs")
    (synopsis
     "Provides utilities for file appenders and making non-blocking writers.
")
    (description
     "This package provides utilities for file appenders and making non-blocking
writers.")
    (license license:expat)))

;;;; END

(define-public rust-pwd-1
  (package
    (name "rust-pwd")
    (version "1.4.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "pwd" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "18p4j95sqqcxn3fbm6gbi7klxp8n40xmcjqy9vz1ww5rg461rivj"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2)
                       ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://gitlab.com/pwoolcoc/pwd.git")
    (synopsis "Safe interface to pwd.h
")
    (description "Safe interface to pwd.h")
    (license unknown-license!)))

;;;;;; END


(define-public rust-file-rotate-0.7
  (package
    (name "rust-file-rotate")
    (version "0.7.5")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "file-rotate" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wcc8q5jb5xv5wr52bl3r1pnd1xxn90kbbjdfv5z65s5xk723wnx"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-chrono" ,rust-chrono-0.4)
                       ("rust-flate2" ,rust-flate2-1))
       #:cargo-development-inputs (("rust-filetime" ,rust-filetime-0.2)
                                   ("rust-quickcheck" ,rust-quickcheck-0.9)
                                   ("rust-quickcheck-macros" ,rust-quickcheck-macros-0.9)
                                   ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://github.com/kstrafe/file-rotate")
    (synopsis "Log rotation for files")
    (description "Log rotation for files")
    (license license:expat)))



;;; END



(define-public rust-konst-proc-macros-0.2
  (package
    (name "rust-konst-proc-macros")
    (version "0.2.11")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "konst_proc_macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dxp8mdh3q9d044ql203way4fgbc50n3j3pi2j1x2snlcaa10klq"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/rodrimati1992/konst/")
    (synopsis "Implementation detail of the `konst` crate")
    (description "Implementation detail of the `konst` crate")
    (license license:zlib)))

(define-public rust-konst-macro-rules-0.2
  (package
    (name "rust-konst-macro-rules")
    (version "0.2.19")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "konst_macro_rules" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0dswja0dqcww4x3fwjnirc0azv2n6cazn8yv0kddksd8awzkz4x4"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t))
    (home-page "https://github.com/rodrimati1992/konst/")
    (synopsis "Implementation detail of the konst crate")
    (description "Implementation detail of the konst crate")
    (license license:zlib)))

(define-public rust-konst-0.2
  (package
    (name "rust-konst")
    (version "0.2.19")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "konst" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "1x3lxxk9vjaiiaabngv7ki2bv9xi36gnqzjzi0s8qfs8wq9hw3rk"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-konst-macro-rules" ,rust-konst-macro-rules-0.2)
                       ("rust-konst-proc-macros" ,rust-konst-proc-macros-0.2)
                       ("rust-trybuild" ,rust-trybuild-1))))
    (home-page "https://github.com/rodrimati1992/konst/")
    (synopsis
     "Const equivalents of std functions, compile-time comparison, and parsing")
    (description
     "Const equivalents of std functions, compile-time comparison, and parsing")
    (license license:zlib)))

(define-public rust-const-format-proc-macros-0.2
  (package
    (name "rust-const-format-proc-macros")
    (version "0.2.32")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "const_format_proc_macros" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0015dzbjbd773nn6096dwqv11fm8m3gy4a4a56cz5x10zl4gzxn7"))))
    (build-system cargo-build-system)
    (arguments
     `(#:skip-build? #t
       #:cargo-inputs (("rust-proc-macro2" ,rust-proc-macro2-1)
                       ("rust-quote" ,rust-quote-1)
                       ("rust-syn" ,rust-syn-1)
                       ("rust-unicode-xid" ,rust-unicode-xid-0.2))))
    (home-page "https://github.com/rodrimati1992/const_format_crates/")
    (synopsis "Implementation detail of the `const_format` crate")
    (description "Implementation detail of the `const_format` crate")
    (license license:zlib)))

(define-public rust-const-format-0.2
  (package
    (name "rust-const-format")
    (version "0.2.32")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "const_format" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "0wvns8mzqwkyciwr00p2g5g4ak7zz8m473di85srj11xmz3i98p3"))))
    (build-system cargo-build-system)
    (arguments
     `(
        #:tests? #f
        #:cargo-inputs (("rust-const-format-proc-macros" ,rust-const-format-proc-macros-0.2)
                       ("rust-konst" ,rust-konst-0.2))
       #:cargo-development-inputs (("rust-arrayvec" ,rust-arrayvec-0.5)
                                   ("rust-fastrand" ,rust-fastrand-1))))
    (home-page "https://github.com/rodrimati1992/const_format_crates/")
    (synopsis "Compile-time string formatting")
    (description "Compile-time string formatting")
    (license license:zlib)))



;;;; END

(define-public rust-lru-0.9
  (package
    (name "rust-lru")
    (version "0.9.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "lru" version))
       (file-name (string-append name "-" version ".tar.gz"))
       (sha256
        (base32 "05yz4qqx7wxbhgxs5hx22j13g8mv9z3gn2pkspykyq48winx9rvi"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-hashbrown" ,rust-hashbrown-0.13))
       #:cargo-development-inputs (("rust-scoped-threadpool" ,rust-scoped-threadpool-0.1)
                                   ("rust-stats-alloc" ,rust-stats-alloc-0.1))))
    (home-page "https://github.com/jeromefroe/lru-rs")
    (synopsis "A LRU cache implementation")
    (description "This package provides a LRU cache implementation")
    (license license:expat)))


;;;; END


(define-public regreet
  (package
    (name "regreet")
    (version "0.1.1")
    (source (origin
             (method url-fetch)
             (uri "https://github.com/rharish101/ReGreet/archive/refs/tags/0.1.1.tar.gz")
             (sha256
              (base32
               "0sx7gv8f6qa00bjkd3a9mzikkgbpv32d1gzh2jlgwb94vwfcjn56"))))
    (build-system cargo-build-system)
    (arguments
     (list
		#:features `( list
			"gtk4_8"
		)
		#:cargo-inputs `(
			( "rust-chrono" ,rust-chrono-0.4)
			( "rust-clap" ,rust-clap-4)
			( "rust-const-format" ,rust-const-format-0.2)
			( "rust-derivative" ,rust-derivative-2)
			( "rust-file-rotate" ,rust-file-rotate-0.7)
			( "rust-glob" ,rust-glob-0.3)
			( "rust-greetd-ipc" ,rust-greetd-ipc-0.9)
			( "rust-gtk4" ,rust-gtk4-0.5)
			( "rust-lru" ,rust-lru-0.9) ; 0.9 required, 0.12 latest
			( "rust-pwd" ,rust-pwd-1)
			( "rust-regex" ,rust-regex-1) ; listed 1.7
			( "rust-relm4" ,rust-relm4-0.5)
			( "rust-serde" ,rust-serde-1)
			( "rust-shlex" ,rust-shlex-1)
			( "rust-thiserror" ,rust-thiserror-1)
			( "rust-tokio" ,rust-tokio-1)
			( "rust-toml" ,rust-toml-0.6)
			( "rust-tracing" ,rust-tracing-0.1)
			( "rust-tracing-appender" ,rust-tracing-appender-0.2)
			( "rust-tracing-subscriber" ,rust-tracing-subscriber-0.3)
			( "rust-tracker" ,rust-tracker-0.2)
		)
    ))
    (native-inputs
     (list pkg-config))
    (inputs
     (list 
		glib
		gdk-pixbuf
		cairo
		pango
		gtk
    ))
    (home-page "https://github.com/rharish101/ReGreet/tree/main")
    (synopsis "Clean and customizable greeter for @command{greetd}")
    (description
     "A clean and customizable GTK-based @command{greetd} greeter written in Rust using Relm4.
This is meant to be run under a Wayland compositor (like @command{sway}).")
    (license license:gpl3)))


; rust-relm4-0.7
; rust-gtk4-0.7
; rust-pwd-1
; rust-file-rotate-0.7
; rust-const-format-0.2


; rust-libadwaita-sys-0.5
; rust-libadwaita-0.5
; rust-gdk4-sys-0.7
; rust-gio-sys-0.18
; rust-glib-sys-0.18
; rust-gobject-sys-0.18
; rust-gtk4-sys-0.7
; rust-gtk4-macros-0.7
; rust-gsk4-sys-0.7
; rust-gsk4-0.7
; rust-graphene-sys-0.18
; rust-graphene-rs-0.18
; rust-gdk4-0.7
; rust-gio-0.18
; rust-gdk-pixbuf-sys-0.18
; rust-gdk-pixbuf-0.18
; rust-glib-macros-0.18
; rust-gobject-sys-0.18
; rust-glib-0.18
; rust-cairo-sys-rs-0.18
; rust-cairo-rs-0.18




; rust-pango-0.18
; rust-pango-sys-0.18

regreet
