( define-module
	( lucyChannel packages hyprlandPlugins)
	#:use-module ( guix gexp)
	#:use-module ( guix git-download)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module ( guix packages)
	#:use-module ( guix build utils)
	#:use-module ( gnu packages wm)

	#:use-module (
		( rosenthal packages wm)
		#:prefix rosenthal.
	)

	#:use-module ( gnu packages build-tools)
	#:use-module ( gnu packages cmake)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build meson-build-system)
	#:use-module ( guix download)

	#:use-module ( gnu packages gtk)
	#:use-module ( gnu packages xdisorg)
	#:use-module ( gnu packages freedesktop)
	#:use-module ( gnu packages pkg-config)
	#:use-module ( gnu packages gcc)
	#:export (
		hycov
		splitMonitorWorkspaces
		hyprlandPlugins
	)
)


( define
	hyprlandPlugins
	( let
		(
			(
				hyprlandPluginSource
				( origin
					( method git-fetch)
					( uri
						( git-reference
							( url "https://github.com/hyprwm/hyprland-plugins")
							( commit "e9457e08ca3ff16dc5a815be62baf9e18b539197")
						)
					)
					( sha256
						( base32 "0rnn5mr8yy98nyyy6q7l12jpr5gzs2904ywypy89p52fzxxb5zm9")
					)
				)
			)
		)
		( map
			( lambda
				( name)
				( cons
					name
					( package
						( name name)
						( version "0.1")
						( home-page "https://github.com/hyprwm/hyprland-plugins")
						( synopsis "")
						( description "")
						( license #f)
						( source
							( file-append
								hyprlandPluginSource
								( string-append
									"/"
									name
								)
							)
						)
						( build-system meson-build-system)
						( native-inputs
							( list
								gcc-13
								libdrm
								libinput
								pango
								; wayland-protocols
								pkg-config
								; cmake
								rosenthal.hyprland
								cairo;-for-hyprland
								; wlroots

								; ( @@
								; 	( rosenthal packages wm)
								; 	wlroots-for-hyprland
								; )

								rosenthal.hyprland-protocols
								rosenthal.hyprlang
								; rosenthal.hyprlang
							)
						)
						( arguments
							( list
								#:phases #~( modify-phases
									%standard-phases
									( replace
										'install
										( lambda
											_
											( copy-file
												( string-append
													"lib"
													( ungexp name)
													".so"
												)
												#$output
											)
										)
									)
								)
							)
						)
					)
				)
			)
			( list
				"borders-plus-plus"
				"csgo-vulkan-fix"
				"hyprbars"
				"hyprexpo"
				"hyprtrails"
				"hyprwinwrap"
			)
		)
	)
)


( define
	hycov
	( package
		( name "hycov")
		( version "0.40.0.1")
		; ( version "0.39.1.1")
		; ( version "0.35.0.1")
		( home-page "")
		( synopsis "")
		( description "")
		( license #f)
		( source
			( origin
				( method git-fetch)
				( uri
					( git-reference
						( url "https://github.com/DreamMaoMao/hycov")
						( commit version)
					)
				)
				( sha256
					( base32 "0yzkzsz2hy31bnb08qc5r4pigpdxm7nmlgp2nbq6b602vf7qdhrx")
					; ( base32 "1ja18247bm9z1p6zbibrmkjayxk9fia4jfk3wsm5m866717bj0j6")
				)
			)
		)
		( build-system meson-build-system)
		( native-inputs
			( list
				gcc-13
				libdrm
				libinput
				pango
				; wayland-protocols
				pkg-config
				cmake
				rosenthal.hyprland
				cairo;-for-hyprland
				; wlroots

				; ( @@
				; 	( rosenthal packages wm)
				; 	wlroots-for-hyprland
				; )

				rosenthal.hyprland-protocols
				rosenthal.hyprlang
			)
		)
		( arguments
			( list
				#:phases #~( modify-phases
					%standard-phases
					( replace
						'install
						( lambda
							_
							; ( display ( getcwd))
							( copy-file
								"libhycov.so"
								#$output
							)
						)
					)
				)
			)
		)
	)
)
( define
	splitMonitorWorkspaces
	( package
		( name "splitMonitorWorkspaces")
		( version "1.1.0")
		( home-page "")
		( synopsis "")
		( description "")
		( license #f)
		( source
			( origin
				( method git-fetch)
				( uri
					( git-reference
						( url "https://github.com/Duckonaut/split-monitor-workspaces")
						( commit "b0ee3953eaeba70f3fba7c4368987d727779826a")
						; ( commit "d5f794be54412e1ae710c8b9e0e27f4835131040")
						; ( commit "2b1abdbf9e9de9ee660540167c8f51903fa3d959")
					)
				)
				( sha256
					( base32 "0vmq3fn1ibj7ddfc3q3134mbsi2zvfzah2z8zih96yb2h14wdn9k")
					; ( base32 "0cvcxdmhyi60l0jsnd44x8xrqagn36s7ry7icd8j5yvgqsp26iv6")
					; ( base32 "1qn01v7r63l0n2j13b6h320hakv2m5gknnxl5jp13fd7czpvwdfc")
				)
			)
		)
		( build-system meson-build-system)
		( native-inputs
			( list
				gcc-13
				libdrm
				libinput
				pango
				; wayland-protocols
				pkg-config
				; cmake
				rosenthal.hyprland
				rosenthal.hyprlang
				; cairo;-for-hyprland
				; wlroots

				; ( @@
				; 	( rosenthal packages wm)
				; 	wlroots-for-hyprland
				; )

				rosenthal.hyprland-protocols
			)
		)
		( arguments
			( list
				#:phases #~( modify-phases
					%standard-phases
					( replace
						'install
						( lambda
							_
							; ( display ( getcwd))
							( copy-file
								"libsplit-monitor-workspaces.so"
								#$output
							)
						)
					)
				)
			)
		)
	)
)

; ( assoc-ref
; 	hyprlandPlugins
; 	; "borders-plus-plus"
; 	; "csgo-vulkan-fix"
; 	; "hyprbars"
; 	"hyprexpo"
; 	; "hyprtrails"
; 	; "hyprwinwrap"
; )

hycov
; splitMonitorWorkspaces
