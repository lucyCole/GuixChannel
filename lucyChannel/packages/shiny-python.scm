( define-module
	( lucyChannel packages shiny-python)
	#:use-module ( guix git-download)
	#:use-module ( guix download)
	#:use-module ( guix gexp)
	#:use-module ( guix utils)
	#:use-module ( gnu packages)
	#:use-module ( gnu)
	#:use-module ( guix packages)
	#:use-module ( guix transformations)
	#:use-module ( guix build-system meson)
	#:use-module ( guix build-system python)
	#:use-module ( guix build utils)
	#:use-module ( guix modules)
	#:use-module ( srfi srfi-1)
	#:use-module ( srfi srfi-26)
	#:use-module (
		( srfi srfi-1)
		#:hide ( zip)
	)
	#:use-module (
		( guix licenses)
		#:prefix license.
	)
	#:use-module ( gnu packages python-build)

	#:use-module (
		( gnu packages build-tools)
		#:select (
			( meson . oldMeson)
		)
	)

	#:use-module ( 
		( gnu packages glib)
		#:select (
			dbus
		)
	)
	#:use-module ( gnu packages python)

	#:export (
		python-3.12
		python-3.12-NonDetermenistic
		python-3.12-withSetuptools
		wrapPython3
		python-3.12-wrapper
		python312-setuptools

		makeInsane
; 		replacePythonBuildSystem
; 		replacePythonInputs
; 		replacePython
	)
)
( define
	python-3.12
	( package
		( inherit python-3.10)
		( name "python")
		( version "3.12.0")
		( source
			( origin
				( inherit
					( package-source python-3.10)
				)
				( uri
					( string-append
						"https://www.python.org/ftp/python/"
						version
						"/Python-"
						version
						".tar.xz"
					)
				)
				( sha256
					( base32 "0p8mrf62w8gg9nhjqhfd4haqfwf62lfcgj0hjydhwnpl9ps38p3r")
				)
				( patches
					( search-patches
						"python-312-fix-tests.patch"
					)
				)
			)
		)
		( arguments
			( append
				( list
					#:tests? #false
				)
				( substitute-keyword-arguments
					( package-arguments python-3.10)
					(
						( #:configure-flags flags)
						`( append
							( lset-difference
								eqv?
								,flags
								( list
									"--with-system-ffi" ; now default
									; "--with-ensurepip=install"
								)
							)
							( list
								; "--with-ensurepip=no"
							)
						)
					)
					(
						( #:modules _)
						`(
							( ice-9 ftw)
							( ice-9 match)
							( guix build utils)
							( guix build gnu-build-system)
							( rnrs lists)
							( srfi srfi-1)
						)
					)
					(
						( #:phases phases)
						`( modify-phases
							,phases
							( delete 'remove-tests)
							( delete 'rebuild-bytecode)
							( replace
								'install-sitecustomize.py
								,( customize-site version)
							)
						)
					)
				)
			)
		)
		( native-inputs
			( if
				( %current-target-system)
				( modify-inputs
					( package-native-inputs python-3.10)
					( replace "python" this-package)
				)
				( package-native-inputs python-3.10)
			)
		)
		( native-search-paths
			( list
				( guix-pythonpath-search-path version)
				;; Used to locate tzdata by the zoneinfo module introduced in
				;; Python 3.9.
				( search-path-specification
					( variable "PYTHONTZPATH")
					( files
						( list "share/zoneinfo")
					)
				)
			)
		)
	)
)

( define
	python-3.12-NonDetermenistic
	( package
		( inherit python-3.12)
		( version "3.12.0-NonDetermenistic")
		( arguments
			( substitute-keyword-arguments
				( package-arguments python-3.12)
				(
					( #:configure-flags flags)
					`( append
						,flags
						( list
							"--with-lto"
							"--enable-optimizations"
						)
					)
				)
			)
		)
	)
)
( define
	wrapPython3
	( @@
		( gnu packages python)
		wrap-python3
	)
)

( define
	python-3.12-wrapper
	( wrapPython3 python-3.12)
)

( define
	python312-setuptools
	( package/inherit
		python-setuptools
		( arguments
			( ensure-keyword-arguments
				( package-arguments python-setuptools)
				`(
					#:python ,python-3.12-wrapper
				)
			)
		)
	)
)
( define
	python-3.12-withSetuptools
	( package/inherit
		python-3.12
		( version "3.12.0-withSetuptools")
		( propagated-inputs
			( modify-inputs
				( package-propagated-inputs python-3.12)
				( append python312-setuptools)
			)
		)
	)
)


( define
	makeInsane
	( lambda
		( sane)
		( package
			( inherit sane)
			( arguments
				( ensure-keyword-arguments
					( package-arguments sane)
					( list
						#:phases `( modify-phases
							,( let
								(
									(
										phases
										( memq
											#:phases
											( package-arguments sane)
										)
									)
								)
								( if
									phases
									( car
										( cdr phases)
									)
									'%standard-phases
								)
							)
							( delete 'sanity-check)
						)
					)
				)
				; ( let
				; 	(
				; 		(
				; 			phases
				; 			( memq
				; 				#:phases
				; 				( package-arguments sane)
				; 			)
				; 		)
				; 	)
				; 	( if
				; 		phases
				; 		( ensure-keyword-arguments
				; 			( package-arguments sane)
				; 			( list
				; 				#:phases `( modify-phases
				; 					,( car
				; 						( cdr phases)
				; 					)
				; 					( delete 'sanity-check)
				; 				)
				; 			)
				; 		)

				; 		( package-arguments sane)
				; 	)
				; )
			)
		)
	)
)


; ( define
; 	(
; ; 		replacePythonInInputs
; 		inputs
; 	)
; 	( modify-inputs
; 		inputs
; 		( replace
; 			"python"
; 			python-3.12
; 		)
; 	)
; )

; ( display 'edjk)
; ( define
; ; 	replacePythonBuildSystema
; 	( createBuildSystemReplacement
; 		python-build-replacement
; 		( list
; 			python312-setuptools
; 			python-setuptools
; 		)
; 		( list
; 			python312-setuptools
; 		)
; 		`(
; 			#:python ,( wrap-python3 python-3.12)
; 		)
; 	)
; )
; ; the name replacement is stupid
; ; inherited from shit but not thought about
; ( define
; ; 	replacePythonBuildSystem
; 	( let
; 		(
; 			(
; 				newPrefix
; 				"python312-"
; 			)
; 			(
; 				oldPrefix
; 				"python-"
; 			)
; 		)
; 		( package-mapping
; 			( lambda
; 				( package)
; 				( if
; 					; ( eqv?
; 					; 	( package-name package)
; 					; 	"python312-setuptools"
; 					; )
; 					( or
; 						( eq?
; 							package
; 							python312-setuptools
; 						)
; 						( eq?
; 							package
; 							python-setuptools
; 						)
; 						; ( eq?
; 						; 	package
; 						; 	python-3.12
; 						; )
; 						; ( eq?
; 						; 	package
; 						; 	python-3.10
; 						; )
; 					)
; 					package
; 					( let
; 						(
; 							(
; 								replacedBuildSystem
; 								( if
; 									( eq?
; 										( package-build-system package)
; 										python-build-system
; 									)
; 									; ( begin
; 									; ( display "REWRITTEN")
; 									; ( display ( package-name package))

; 									( package/inherit
; 										package
; 										( location
; 											( package-location
; 												package
; 											)
; 										)
; 										( name
; 											( let
; 												(
; 													(
; 														name
; 														( package-name package)
; 													)
; 												)
; 												( string-append
; 													newPrefix
; 													( if
; 														( string-prefix?
; 															oldPrefix
; 															name
; 														)
; 														( substring
; 															name
; 															( string-length oldPrefix)
; 														)
; 														name
; 													)
; 												)
; 											)
; 										)
; 										( inputs
; 											( modify-inputs
; 												( package-inputs package)
; 												; ( append python-setuptools)
; 												( append python312-setuptools)
; 											)
; 										)
; 										( arguments
; 											( ensure-keyword-arguments
; 												( package-arguments package)
; 												`(
; 													#:python ,( wrap-python3 python-3.12)
; 												)
; 											)
; 											; ( let
; 											; 	(
; 											; 		(
; 											; 			python
; 											; 			( if
; 											; 				( promise? python)
; 											; 				( force python)
; 											; 				python
; 											; 			)
; 											; 		)
; 											; 	)
; 											; 	( ensure-keyword-arguments
; 											; 		( package-arguments package)
; 											; 		`( #:python ,python)
; 											; 	)
; 											; )
; 										)
; 									; )
; 									)
; 									package
; 								)
; 							)
; 						)
; 						replacedBuildSystem
; 						; ( package/inherit
; 						; 	replacedBuildSystem
; 						; 	( inputs
; ; 						; 		( replacePythonInInputs
; 						; 			( package-inputs package)
; 						; 		)
; 						; 	)
; 						; 	( native-inputs
; ; 						; 		( replacePythonInInputs
; 						; 			( package-native-inputs package)
; 						; 		)
; 						; 	)
; 						; 	( propagated-inputs
; ; 						; 		( replacePythonInInputs
; 						; 			( package-propagated-inputs package)
; 						; 		)
; 						; 	)
; 						; )
; 					)
; 				)
; 			)
; 		)
; 	)
; )

; ( define
; ; 	replacePythonInputs
; 	( package-input-rewriting/spec
; 		`(
; 			( "python" . ,( const python-3.12))
; 		)
; 	)
; )
; ( define
; 	(
; ; 		replacePython
; 		package
; 	)
; ; 	( replacePythonInputs
; ; 		( replacePythonBuildSystem package)
; 	)
	
; )

; so they mentioned using ( wrap-python3)
; ; ( replacePython python-setuptools)



; so python is replaced
; this is then used to build 
; also want to replace python with one bundled with setuptools
; so can create python which propagates setuptools

; (
; 	define
; )

; then want to replace python with this in other packages
; is this to replace the one used in the build system or not
; just need setuptools in inputs

; want one function to apply
; the build system python can be any python3.12
; new setuptools must be present in native input-(.)
; so walk given package input tree
; all packages which 

; so need to add setuptools to build system input-(.)


; python312-setuptools
; ; ( replacePython oldMeson)
; ( debugMeson
; 	"UNCHANGED"
; 	oldMeson
; )
; ( debugMeson
; 	"REPLACED BUILD SYSTEM"
; ; 	( replacePythonBuildSystem
; 		oldMeson
; 	)
; )
; ( debugMeson
; 	"REPLACED INPUTS"
; ; 	( replacePythonInputs
; 		oldMeson
; 	)
; )
; ( debugMeson
; 	"REPLACED ALL"
; ; 	( replacePython
; 		oldMeson
; 	)
; )
; ( define
; ; 	replacePython
; 	( options->transformation
; 		'(
; 			( with-input . "python=python@3.12.0")
			
; 		)
; 	)
; )
; 	; ( package-input-rewriting
; 	; 	`(
; 	; 		; ( python-3.10 . python-3.12)
; 	; 		( "python" . ,python-3.12)
; 	; 	)
; 	; 	; ( lambda
; 	; 	; 	( oldName)
; 	; 	; 	( string-append
; 	; 	; 		oldName
; 	; 	; 		"-WithPython-3.12"
; 	; 	; 	)
; 	; 	; )
; 	; )
; )
; ensurepip used to install setuptools and no longer does so
; ( define
; 	( withSetupTools oldPackage)
; 	( package
; 		( inherit oldPackage)
; 		( inputs
; 			( modify-inputs
; 				( package-inputs oldPackage)
; 				( append
; ; 					( replacePython
; 						python-setuptools
; 					)
; 				)
; 			)
; 		)
; 	)
; )
; ( define
; 	setuptools-12
; ; 	( replacePython
; 		( package
; 			( inherit python-setuptools)
; 			; ( name "python-setuptools-312")
; 			( name "python-setuptools-312")
; 		)
;	)
;)
		
		; ( package
		; 	( inherit python-setuptools)
		; 	( name "yup")
		; 	; ( inputs
		; 	; 	( modify-inputs
		; 	; 		( package-inputs python-setuptools)
		; 	; 		( replace
		; 	; 			python-3.10
		; 	; 			python-3.12
		; 	; 		)
		; 	; 	)
		; 	; )
		; 	; ( arguments
		; 	; 	( append
		; 	; 		( substitute-keyword-arguments
		; 	; 			( package-arguments python-setuptools)
		; 	; 			; (
		; 	; 			; 	( #:python _)
		; 	; 			; 	python-3.12
		; 	; 			; )
		; 	; 		)
		; 	; 		( list
		; 	; 			#:python python-3.12
		; 	; 		)
		; 	; 	)
		; 	; )
		; )
; 	)
; )
; ( define
; 	python-3.12-Setuptools
; 	( package
; 		( inherit python-3.12)
; 		( version "3.12.0-Setuptools")
; 		; ( version "yuiop")
; 		( propagated-inputs
; 			( modify-inputs
; 				( package-propagated-inputs python-3.12)
; 				( append
; 					; setuptools-12
; ; 					( replacePython
; 						python-setuptools
; 					)
; 				)
; 			)
; 		)
; 	)
; )
; ( define
; ; 	replacePythonBackCompat
; 	( options->transformation
; 		'(
; 			( with-input . "python=python@yuiop")
; 		)
; 	)
; )

; ( define
; 	meson
; ; 	( replacePython
; 		( withSetupTools oldMeson)
; 	)
; )
; meson

; ( use-modules
; 	( guix build-system python)
; 	( guix transformations)
; )

; (define* (package-with-explicit-python python old-prefix new-prefix
;                                        #:key variant-property)
;   "Return a procedure of one argument, P.  The procedure creates a package with
; the same fields as P, which is assumed to use PYTHON-BUILD-SYSTEM, such that
; it is compiled with PYTHON instead.  The inputs are changed recursively
; accordingly.  If the name of P starts with OLD-PREFIX, this is replaced by
; NEW-PREFIX; otherwise, NEW-PREFIX is prepended to the name.

; When VARIANT-PROPERTY is present, it is used as a key to search for
; pre-defined variants of this transformation recorded in the 'properties' field
; of packages.  The property value must be the promise of a package.  This is a
; convenient way for package writers to force the transformation to use
; pre-defined variants."
;   (define package-variant
;     (if variant-property
;         (lambda (package)
;           (assq-ref (package-properties package)
;                     variant-property))
;         (const #f)))

;   (define (transform p)
;     (cond
;      ;; If VARIANT-PROPERTY is present, use that.
;      ((package-variant p)
;       => force)

;      ;; Otherwise build the new package object graph.
;      ((eq? (package-build-system p) python-build-system)
;       (package/inherit p
;         (location (package-location p))
;         (name (let ((name (package-name p)))
;                 (string-append new-prefix
;                                (if (string-prefix? old-prefix name)
;                                    (substring name
;                                               (string-length old-prefix))
;                                    name))))
;         (arguments
;          (let ((python (if (promise? python)
;                            (force python)
;                            python)))
;            (ensure-keyword-arguments (package-arguments p)
;                                      `(#:python ,python))))))
;      (else p)))

;   (define (cut? p)
;     (or (not (eq? (package-build-system p) python-build-system))
;         (package-variant p)))

;   (package-mapping transform cut?))
; ; ; ( ( package-with-explicit-python python-3.12 "python-" "python3.12-") ( replacePython python-setuptools))
; ; setuptools-12
; ; (
; ; 	( options->transformation
; ; 		'(
; ; 			( with-input . "python=python@3.12.0")
			
; ; 		)
; ; 	)
; ; 	python-setuptools
; ; )
; ; ; ( replacePython python-setuptools)
