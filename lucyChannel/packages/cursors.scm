( define-module
	
	( lucyChannel packages gaphor)
	#:use-module ( guix gexp)
	#:use-module (
		( guix licenses)
		#:prefix license:
	)
	#:use-module ( guix packages)
	#:use-module ( guix build utils)
	#:use-module ( guix build-system copy)
	#:use-module ( guix download)
	#:use-module ( guix utils)
	#:export (
		cursors
	)
)
( define
	cursors
	( list
		( cons
			"hyprCursor"
			( list
				( package
					( name "rosePine")
					( version "0.3.2")
					( source
						( origin
							( method url-fetch)
							( uri
								( string-append
									"https://github.com/ndom91/rose-pine-hyprcursor/releases/download/v"
									version
									"/rose-pine-cursor-hyprcursor_"
									version
									".tar.gz"
								)
							)
							( sha256
								( base32 "13mm26vi5ir50fzqhkbd6qv0nnz4yh88gibv65jw88zpgvjaz12y")
							)
						)
					)
					( build-system copy-build-system)
					( arguments
						`(
							#:install-plan `(
								( ".." "share/icons/rose-pine-hyprcursor")
							)
 						; 	#:phases ,#~( modify-phases
							; 	%standard-phases
							; 	( add-after
							; 		'patch-generated-file-shebangs
							; 		'qq
							; 		( lambda
							; 			_
							; 			( use-modules
							; 				( ice-9 ftw)
							; 			)
							; 			( display
							; 				( scandir "..")
							; 			)
							; 			( newline)
							; 		)
							; 	)
							; )
						)
					)
					( home-page "https://github.com/ndom91/rose-pine-hyprcursor")
					( synopsis "")
					( description "")
					( license #f)
				)
			)
		)
	)
)
( list-ref
	( assoc-ref
		cursors
		"hyprCursor"
	)
	0
)
