( define-module
	( lucyChannel packages guile)
	#:use-module ( gnu packages)
	#:use-module ( guix packages)
	#:use-module ( guix git-download)
	#:use-module ( guix build-system copy)
	#:use-module ( gnu packages guile)
	#:use-module ( gnu packages pkg-config)
	#:use-module (
		( guix licenses)
		#:prefix license:
	)
	#:export (
		guile-toml-0.1
	)
)

( define
	guile-toml-0.1
	( package
		( name "guile-toml")
		( version "0.1")
		( source
			( origin
				( method git-fetch)
				( uri
					( git-reference
						( url "https://github.com/hylophile/guile-toml")
						( commit "ecb24deb407ef76ef7cf7e9f0115060c98366a6b")
					)
				)
				( sha256
					( base32 "1iqxivxcb0dcjbd5ba3c0g3mj71b32qjxfdmljyv9r297yykfd02")
				)
			)
		)
		( build-system copy-build-system)
		( arguments
			`(
				#:install-plan `(
					( "toml" "/share/guile/site/3.0/toml")
					( "toml.scm" "/share/guile/site/3.0/toml.scm")
				)
			)
		)
		( native-inputs
			( list
				pkg-config
				guile-3.0
			)
		)
		( inputs
			( list
				guile-3.0
			)
		)
		( home-page "https://github.com/hylophile/guile-toml")
		( synopsis "TOML for guile; v1.0.0 compliant")
		( description "")
		( license license:gpl3+)
	)
)
